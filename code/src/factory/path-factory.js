/* eslint-disable no-param-reassign */
import { Path, Color } from 'paper';

export const makePath = () => {
  const path = new Path({
    strokeColor: new Color('#ffffff'),
    strokeWidth: 0,
    strokeCap: 'butt',
    strokeJoin: 'round',
    selected: true,
  });
  path.name = `Path ${path.id}`;
  path.neon = {};
  return path;
};

const styleLikeAnError = (item) => {
  item.strokeColor = '#ffffff';
  item.dashArray = [1, 2];
};

export const makePolygon = (point, sides) => {
  const arc = new Path.RegularPolygon(point, sides, /* radius= */ 25); // Radius is the same as Path-Angle's arc.
  styleLikeAnError(arc);
  return arc;
};

export const makeCircle = (point) => {
  const arc = new Path.Circle(point, /* radius= */ 25); // Radius is the same as Path-Angle's arc.
  styleLikeAnError(arc);
  return arc;
};

export const makeLine = (from, to) => {
  const arc = new Path.Line(from, to);
  styleLikeAnError(arc);
  return arc;
};

export const makeArc = (from, through, to) => {
  const arc = new Path.Arc(from, through, to);
  styleLikeAnError(arc);
  return arc;
};

/* eslint-disable default-param-last */
/* eslint-disable no-param-reassign */
import { PointText } from 'paper';

// Thanks! https://stackoverflow.com/questions/16534659/draw-text-along-the-bezier-curve-in-paper-js

// Create a PointText object for a string and a style
export const createPointText = (str, style = { fillColor: 'white' }) => {
  const text = new PointText();
  text.content = str;
  if (style) {
    if (style.font) { text.font = style.font; }
    if (style.fontFamily) { text.fontFamily = style.fontFamily; }
    if (style.fontSize) { text.fontSize = style.fontSize; }
    if (style.fontWieght) { text.fontWeight = style.fontWeight; }
    if (style.fillColor) { text.fillColor = style.fillColor; }
  }
  return text;
};

// Make a string follow a path.
export const createAlignedText = (str, path, options = {}, style) => {
  // create PointText object for each glyph
  const glyphTexts = str.split('').map((letter) => {
    const text = createPointText(letter, style);
    text.justification = 'center';
    return text;
  });
  // for each glyph find center xOffset
  const xOffsets = glyphTexts.reduce((acc, cur) => [...acc, (acc.at(-1) || 0) + cur.bounds.width], []);

  glyphTexts.forEach((glyph, i) => {
    const xOffset = xOffsets[i] - (glyph.bounds.width / 2);
    if (xOffset > path.length) {
      return;
    }

    const textOffset = options.mirrorOnLine ? (path.length - xOffset) : xOffset;
    const pathPoint = path.getPointAt(textOffset);
    const tan = path.getTangentAt(textOffset);

    glyph.point = pathPoint;
    glyph.rotate((tan?.angle || 0) + (options.mirrorOnLine ? 180 : 0), pathPoint);
  });

  return glyphTexts;
};

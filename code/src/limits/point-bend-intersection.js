/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
/* eslint-disable consistent-return */
import { makePolygon } from '../factory/path-factory.js';
import { createPointText } from '../factory/text.js';
import { scaleMmToPixels } from '../paper/scale-conversion.js';
import { getRadiusInMm } from '../tools/point-radius.js';

const drawIntersection = (point) => {
  const circle = makePolygon(point, 3);
  const text = createPointText('Incompatable with bend radius');
  text.point = point.add(-(text.bounds.width / 2.0), -circle.bounds.height);
};

const checkIntersections = (paths) => {
  paths.forEach((path) => {
    path.segments
      .filter((segment) => !segment.isFirst() && !segment.isLast())
      .forEach((segment) => {
        const radiusMm = getRadiusInMm(path, segment.index);

        if (!radiusMm) {
          return;
        }

        const { previous, next } = segment;
        const distAPx = previous.point.getDistance(segment.point);
        const distBPx = next.point.getDistance(segment.point);

        const radiusPx = scaleMmToPixels(radiusMm);

        if (radiusPx > distAPx) {
          drawIntersection(previous.point);
        }

        if (radiusPx > distBPx) {
          drawIntersection(next.point);
        }
      });
  });
};

// Prevents a bend from being larger than its endpoints.
export class PointBendIntersection {
  get name() {
    return 'point bend intersection';
  }

  check(paths) {
    checkIntersections(paths);
  }
}

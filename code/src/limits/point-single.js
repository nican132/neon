/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
/* eslint-disable consistent-return */
import { makePolygon } from '../factory/path-factory.js';
import { createPointText } from '../factory/text.js';

const drawPoint = (point) => {
  const circle = makePolygon(point, 8);
  const text = createPointText('Path must have at least two points');
  text.point = point.add(-(text.bounds.width / 2.0), -circle.bounds.height);
};

// Prevents a path with just a single point.
export class PointSingle {
  get name() {
    return 'point-single';
  }

  check(paths) {
    paths.filter((path) => path.segments.length === 1).forEach((path) => drawPoint(path.segments.at(0).point));
  }
}

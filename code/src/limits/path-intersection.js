/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
/* eslint-disable consistent-return */
import { makePolygon } from '../factory/path-factory.js';
import { createPointText } from '../factory/text.js';

const drawIntersection = (point) => {
  const circle = makePolygon(point, 4);
  const text = createPointText('Too many intersections');
  text.point = point.add(-(text.bounds.width / 2.0), -circle.bounds.height);
};

const checkIntersections = (paths) => {
  const pathChecks = new Set();

  paths.forEach((pathA) => {
    paths.forEach((pathB) => {
      const pathCheck = [pathA.id, pathB.id].sort().join(',');
      if (pathChecks.has(pathCheck)) {
        return;
      }
      pathChecks.add(pathCheck);

      const intersections = pathA.getIntersections(pathB);
      if (intersections.length === 0) {
        return;
      }

      intersections
        .filter((intersection) => intersection.offset !== 0
          && intersection.index !== 0
          && intersection.curveOffset !== 0)
        .forEach((intersection) => {
          drawIntersection(intersection.point);
        });
    });
  });
};

// Prevents two paths from being too close to each other.
export class PathIntersection {
  get name() {
    return 'path-intersection';
  }

  check(paths, offsets) {
    checkIntersections(offsets);
  }
}

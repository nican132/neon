/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
import { Point } from 'paper';
import { clamp } from 'lodash';

import { makePolygon } from '../factory/path-factory.js';
import { createPointText } from '../factory/text.js';
import { getCanvasWidth } from '../paper/scale-conversion.js';

const OFFSET = 10;

/* eslint-disable no-new */
const drawOffscreen = (point, canvasWidth) => {
  const placement = new Point(clamp(point.x, 0, canvasWidth), clamp(point.y, 0, canvasWidth));
  const rectangle = makePolygon(placement, 4);
  rectangle.rotate(45, placement);

  const text = createPointText('Too far offscreen');
  text.point = rectangle.bounds.center.subtract(text.bounds.center);
  if (point.x > canvasWidth) {
    text.point.x = rectangle.bounds.left - text.bounds.width - OFFSET;
  } else if (point.x < 0) {
    text.point.x = rectangle.bounds.right + OFFSET;
  }

  if (point.y > canvasWidth) {
    text.point.y = rectangle.bounds.top - OFFSET;
  } else if (point.y < 0) {
    text.point.y = rectangle.bounds.bottom + text.bounds.height + OFFSET;
  }
};

const checkOffscreen = (paths) => {
  const canvasWidth = getCanvasWidth();

  paths.forEach((path) => {
    path.segments
      .forEach((segment) => {
        const { point } = segment;

        if (point.x > 0 && point.x < canvasWidth
          && point.y > 0 && point.y < canvasWidth) {
          return;
        }

        drawOffscreen(point, canvasWidth);
      });
  });
};

// Prevents two segments of a path having too sharp of a bend that would break the leds.
export class PointOffscreen {
  get name() {
    return 'point-offscreen';
  }

  check(paths) {
    checkOffscreen(paths);
  }
}

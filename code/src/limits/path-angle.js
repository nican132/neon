/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
/* eslint-disable no-new */
import { Path } from 'paper';

import { createAlignedText } from '../factory/text.js';
import { makeArc } from '../factory/path-factory.js';
import { getRadiusInMm } from '../tools/point-radius.js';

const ILLEGAL_ANGLE_DEGREES = 89.9999; // Anything less than this is bad and will cause kinks.

// Found by plotting what looked like good values, and then finding a quadratic fit
// const getMinRadiusInMm = (x) => 270 - 5.25 * x + 0.025 * x * x;
// Then trying an exponential plot instead, shows slightly better results
const getMinRadiusInMm = (x) => 0.0181 * Math.E ** (0.0602 * (180 - x));

const drawAngle = (center, source, angle, acuteAngle, label) => {
  const radius = 25;

  const from = source.normalize(radius);
  const through = from.rotate(angle / 2).multiply(angle < 180 ? -1 : 1);
  const to = from.rotate(angle);

  makeArc(center.add(from), center.add(through), center.add(to));

  if (label) {
    const size = radius * 2;
    const textArc = new Path.Circle(center, size);
    textArc.rotate(through.angle - 90, center);
    createAlignedText(`Too steep of an angle: ${Math.floor(acuteAngle * 100) / 100}°`, textArc, { mirrorOnLine: true });
  }
};

const checkAngle = (paths) => {
  paths.forEach((path) => {
    path.segments
      .filter((segment) => !segment.isFirst())
      .filter((segment) => !segment.isLast())
      .forEach((curr) => {
        const { previous, next, index } = curr;

        const vecA = previous.point.subtract(curr.point);
        const vecB = curr.point.subtract(next.point);

        const angle = 180 + vecA.getDirectedAngle(vecB);

        if (ILLEGAL_ANGLE_DEGREES < angle && angle < 360 - ILLEGAL_ANGLE_DEGREES) {
          return;
        }

        const acuteAngle = angle > 180 ? (360 - angle) : angle;
        const minRadius = getMinRadiusInMm(acuteAngle);
        const radiusInMm = getRadiusInMm(path, index);

        if (radiusInMm > minRadius) {
          return;
        }

        drawAngle(curr.point, vecA, angle, acuteAngle, /* label= */ true);
      });
  });
};

// Prevents two segments of a path having too sharp of a bend that would break the leds.
export class PathAngle {
  get name() {
    return 'point-angle';
  }

  check(paths) {
    checkAngle(paths);
  }
}

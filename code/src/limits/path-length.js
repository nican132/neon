/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { makeLine, makePolygon } from '../factory/path-factory.js';
import { createPointText } from '../factory/text.js';
import { scaleMmToPixels, scalePixelsToMm } from '../paper/scale-conversion.js';
import { SETTINGS, settingsState } from '../views/components/settings.js';

const ILLEGAL_MAX_LENGTH_MM = 2500; // Anthing longer than this is not allowed.
const ILLEGAL_MIN_LENGTH_FUDGE_MM = 0.00001; // Anthing shorter than this is not allowed.

const ILLEGAL_CUT_LENGTH_MAX_BUFFER_MM = 3.2; // Anything cut bigger than this is not allowed. Its better to be too long than too short.
const ILLEGAL_CUT_LENGTH_MIN_BUFFER_MM = -0.8; // Anything cut smaller than this is not allowed.

const drawLength = (point, message) => {
  const circle = makePolygon(point, 5);
  const text = createPointText(message);
  text.point = point.add(-(text.bounds.width / 2.0), -circle.bounds.height);
};

const drawCuts = (path, neonCutLengthInMm) => {
  const neonCutLengthInPixels = scaleMmToPixels(neonCutLengthInMm);
  const cuts = path.length / neonCutLengthInPixels;
  for (let i = 0; i < cuts; i += 1) {
    const offset = neonCutLengthInPixels * i;
    const point = path.getPointAt(offset);
    const normal = path.getNormalAt(offset).multiply(scaleMmToPixels(10));
    makeLine(point.subtract(normal), point.add(normal));
  }
};

const checkLength = (paths) => {
  paths.forEach((path) => {
    if (path.length <= 0) {
      return;
    }

    const selectedSegment = path.segments.find((segment) => segment.point.selected) || path.segments.at(-1);
    const neonCutLengthInMm = settingsState.getSetting(SETTINGS.NEON_CUT_LENGTH_MM);
    const illegalMinLength = neonCutLengthInMm - ILLEGAL_MIN_LENGTH_FUDGE_MM;

    if (path.neon.parent.selected) {
      drawCuts(path, neonCutLengthInMm);
    }

    const distanceInMm = scalePixelsToMm(path.length);
    if (distanceInMm > ILLEGAL_MAX_LENGTH_MM) {
      // Too long.
      drawLength(selectedSegment.point, `Too long of path ${Math.floor(distanceInMm * 100) / 100}mm`);
      return;
    }

    if (distanceInMm < illegalMinLength) {
      // Too short.
      drawLength(selectedSegment.point, `Too short of path ${Math.floor(distanceInMm * 100) / 100}mm`);
      return;
    }

    const moduloLength = (distanceInMm % neonCutLengthInMm);
    if (moduloLength > ILLEGAL_CUT_LENGTH_MAX_BUFFER_MM && moduloLength < (neonCutLengthInMm + ILLEGAL_CUT_LENGTH_MIN_BUFFER_MM)) {
      drawLength(selectedSegment.point, `Wrong path length ${Math.floor(moduloLength * 100) / 100}mm`);
      return;
    }
  });
};

export class PathLength {
  get name() {
    return 'path-length';
  }

  check(paths, outlines, drawPaths) {
    checkLength(drawPaths);
  }
}

/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import paper from 'paper';
import { PaperOffset } from 'paperjs-offset';

import { scaleMmToPixels } from '../paper/scale-conversion.js';
import { getOrCreateLayer } from '../paper/layer-management.js';

import { SETTINGS, settingsState } from '../views/components/settings.js';

const getOffsetInMm = () => settingsState.getSetting(SETTINGS.NEON_WIDTH_MM) + settingsState.getSetting(SETTINGS.WALL_WIDTH_MM);
const getOutlineInMm = () => settingsState.getSetting(SETTINGS.NEON_WIDTH_MM) + (settingsState.getSetting(SETTINGS.WALL_WIDTH_MM) * 2);

const makeOffsetOrOutline = (drawPaths, strokeInMM, strokeColor = 'Transparent') => {
  const offsets = drawPaths
    .filter((path) => path.segments.length > 1)
    .map((path) => {
      const key = `offset_${strokeInMM}_${strokeColor}`;
      if (path.neon[key]) {
        // Really dumb cache that makes a lot of assumptions
        // and has a lot of side effects. But its desperately needed!
        return path.neon[key];
      }
      const offset = PaperOffset.offsetStroke(path, scaleMmToPixels(strokeInMM / 2), { insert: false, join: 'round', cap: 'butt' });
      path.neon[key] = offset;
      return offset;
    });

  offsets.forEach((item) => {
    paper.project.activeLayer.addChild(item);
    item.fillColor = null;
    item.shadowBlur = null;
    item.shadowColor = null;
    item.shadowOffset = null;
    item.strokeWidth = 1;
    item.strokeColor = strokeColor;

    if (strokeColor === 'Transparent') {
      item.visible = false;
    }
  });

  return offsets;
};

export class Limiter {
  #limiters = [];

  constructor(limiters) {
    this.#limiters = limiters;
  }

  get name() {
    return 'limiter';
  }

  hasConflicts() {
    const limiterLayerNames = this.#limiters.map((limiter) => limiter.name);
    return paper.project.layers.filter((layer) => limiterLayerNames.includes(layer.name))
      .find((layer) => layer.hasChildren());
  }

  checkLimits(interactiveLayer, drawLayer) {
    const currentLayer = paper.project.activeLayer;

    const paths = interactiveLayer.getItems({ className: 'Path' });
    const drawPaths = drawLayer.getItems({ className: 'Path' });

    const limiterLayer = getOrCreateLayer(this.name);
    limiterLayer.activate();
    limiterLayer.removeChildren();

    // We collide on the offset, rather than the outline, to allow the 3d print to overlap
    // just a bit, giving it extra structure and support by sharing walls.
    const offsets = makeOffsetOrOutline(drawPaths, getOffsetInMm()); // Offset hugs the outside of the neon
    const outline = makeOffsetOrOutline(drawPaths, getOutlineInMm(), '#555555'); // Outline hugs the outside of the 3d print

    this.#limiters.filter(() => settingsState.getSetting(SETTINGS.SHOW_LIMITS)).forEach((limiter) => {
      const layer = getOrCreateLayer(limiter.name);

      layer.activate();
      layer.removeChildren();
      limiter.check(paths, offsets, drawPaths);
    });

    currentLayer.activate();
  }

  clearConflicts() {
    getOrCreateLayer(this.name).removeChildren();

    this.#limiters.forEach((limiter) => {
      getOrCreateLayer(limiter.name).removeChildren();
    });
  }
}

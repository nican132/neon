import { html } from 'htm/preact';
import { useEffect, useState } from 'preact/hooks';

import { Tools } from './components/tools';

export const CENTER_ID = 'center-id';

const Body = ({ onClick }) => html`<${Tools} onClick=${onClick} />`;

export const Center = ({ onClick, stateSubscriber }) => {
  const [, setState] = useState({});
  useEffect(() => {
    stateSubscriber?.(CENTER_ID, setState);
  }, [stateSubscriber, setState]);

  return html`<${Body} onClick=${(...args) => onClick(CENTER_ID, ...args)} />`;
};

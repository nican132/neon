import { html } from 'htm/preact';
import { useEffect, useState } from 'preact/hooks';

import { Paths } from './components/paths.js';
import { Buttons } from './components/buttons.js';

export const RIGHT_ID = 'right-id';

const RightBody = ({
  selectedTab, paths, onClick, onChange,
}) => html`
<${Paths} onClick=${onClick} onChange=${onChange} paths=${paths} selectedTab=${selectedTab} />
<${Buttons} onClick=${onClick} />
`;

export const Right = ({ onClick, onChange, stateSubscriber }) => {
  const [state, setState] = useState({});
  useEffect(() => {
    stateSubscriber?.(RIGHT_ID, setState);
  }, [stateSubscriber, setState]);

  return html`<${RightBody} selectedTab=${state.selectedTab} paths=${state.paths} 
    onClick=${(...args) => onClick(RIGHT_ID, ...args)} onChange=${(...args) => onChange(RIGHT_ID, ...args)} />`;
};

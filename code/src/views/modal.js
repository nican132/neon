import { html } from 'htm/preact';
import { useEffect, useState } from 'preact/hooks';
import ReactModal from 'react-modal';

import { Settings } from './components/settings.js';

export const MODAL_ID = 'modal-id';

ReactModal.setAppElement(document.getElementById('main-content'));

const ModalBody = ({
  isSettingsOpen, settings,
  onClick, onChange,
}) => html`
  <${ReactModal}
    isOpen=${isSettingsOpen}
    shouldCloseOnOverlayClick=${true}
    className="Modal"
    overlayClassName="Overlay">
    ${isSettingsOpen ? html`<${Settings} settings=${settings} onClick=${onClick} onChange=${onChange} />` : html``}
  <//>
`;

export const Modal = ({ onClick, onChange, stateSubscriber }) => {
  const [state, setState] = useState({});
  useEffect(() => {
    stateSubscriber?.(MODAL_ID, setState);
  }, [stateSubscriber, setState]);

  return html`<${ModalBody} 
    isSettingsOpen=${state.isSettingsOpen}
    settings=${state.settings}
    onClick=${(...args) => onClick(MODAL_ID, ...args)} 
    onChange=${(...args) => onChange(MODAL_ID, ...args)} 
  />`;
};

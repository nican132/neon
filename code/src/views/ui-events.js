/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
import { UI_EVENT } from './ui.js';
import { RIGHT_ID } from './right.js';
import { LEFT_ID } from './left.js';
import { CENTER_ID } from './center.js';

import { COLOR_SELECT } from './components/colors.js';
import {
  ADD_PATH_SELECT, CHANGE_PATH_SELECT, CHANGE_PATH_TEXT, DUPLICATE_PATH_SELECT, FOCUS_TAB, REMOVE_PATH_SELECT, SMOOTH_PATH_SELECT,
} from './components/paths.js';
import {
  EXPORT_3D_PRINT_BUTTON_SELECT, EXPORT_SVG_BUTTON_SELECT, IMPORT_BACKGROUND_BUTTON_SELECT, SETTINGS_BUTTON_SELECT,
} from './components/buttons.js';
import {
  TOOL_CTRL_SELECT, TOOL_MIRROR_H, TOOL_MIRROR_V, TOOL_MOVE_DOWN,
  TOOL_MOVE_LEFT, TOOL_MOVE_RIGHT, TOOL_MOVE_UP, TOOL_ROTATE_LEFT,
  TOOL_ROTATE_RIGHT, TOOL_SHIFT_SELECT, TOOL_SNAP_SELECT, TOOL_UNDO_SELECT,
} from './components/tools.js';

import { makePath } from '../factory/path-factory.js';

import { restructureGeometry } from '../cad/make-geometry.js';
import { renderTo3mf } from '../cad/make-model.js';
import { deserializeToGeometry, serializeToString } from '../cad/read-svg.js';

import {
  getCutProcessLayer, getInteractiveProcessLayer, getLayerByTab, getSupportProcessLayer,
} from '../paper/layer-management.js';
import { getAllPaths } from '../paper/get-selected-item.js';
import { MODAL_ID } from './modal.js';
import { SETTINGS_ADJUSTED, SETTINGS_BUTTON_ACCEPT } from './components/settings.js';

export const MAX_PATHS = 200;

const saveFile = (filename, data, type) => {
  const blob = new Blob([data], { type });
  if (window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveBlob(blob, filename);
  } else {
    const elem = window.document.createElement('a');
    elem.href = window.URL.createObjectURL(blob);
    elem.download = filename;
    document.body.appendChild(elem);
    elem.click();
    document.body.removeChild(elem);
  }
};

const leftSideEvent = (activity, args) => {
  if (activity === COLOR_SELECT) {
    const path = getAllPaths().find((p) => p.selected);
    path.strokeColor = args[1];
  }
};

const rightSideEvent = (activity, args, settingsState) => {
  if (activity === CHANGE_PATH_SELECT) {
    const paths = getAllPaths();
    const path = paths.find((p) => p.id === args[1]);
    paths.forEach((p) => { p.selected = false; });
    path.selected = true;
  }

  if (activity === ADD_PATH_SELECT) {
    const paths = getAllPaths();
    if (paths.length >= MAX_PATHS) {
      alert('You cannot add another path to the project.');
      return;
    }

    paths.forEach((path) => { path.selected = false; });
    makePath();
  }

  if (activity === REMOVE_PATH_SELECT) {
    const paths = getAllPaths();
    if (paths.length === 1) {
      if (paths[0].segments.length > 0) {
        paths[0].removeSegments();
        return;
      }

      alert('You must have at least one path in project.');
      return;
    }

    const path = paths.find((p) => p.id === args[1]);
    const isSelected = path.selected;
    path.remove();

    if (isSelected) {
      paths.find((p) => p.id !== args[1]).selected = true;
    }
  }

  if (activity === SMOOTH_PATH_SELECT) {
    alert('Path smoothing is no longer supported.');
  }

  if (activity === DUPLICATE_PATH_SELECT) {
    const paths = getAllPaths();
    if (paths.length >= MAX_PATHS) {
      alert('You cannot add another path to the project.');
      return;
    }

    paths.forEach((path) => { path.selected = false; });
    const path = paths.find((p) => p.id === args[1]);
    const clone = makePath();
    clone.importJSON(path.exportJSON()); // All other clone methods aren't deep.
    clone.name = `${clone.id} duplicate`;
    clone.selected = true;
  }

  if (activity === EXPORT_SVG_BUTTON_SELECT) {
    // This needs to be updated to use Supports if it gets re-enabled.
    const paths = getInteractiveProcessLayer().getItems({ className: 'Path' });
    const svgString = serializeToString(paths);
    const filename = `neon_${(new Date().toJSON().slice(0, 10))}_${Date.now()}.svg`;
    saveFile(filename, svgString, /* type= */ 'image/svg+xml');
  }

  if (activity === EXPORT_3D_PRINT_BUTTON_SELECT) {
    const buildGeometry = (layer) => {
      const paths = layer.getItems({ className: 'Path' });
      const svgString = serializeToString(paths);
      return deserializeToGeometry(svgString);
    };
    const svgGeom = buildGeometry(getInteractiveProcessLayer());
    const svgSupportGeom = buildGeometry(getSupportProcessLayer());
    const svgCutGeom = buildGeometry(getCutProcessLayer());
    const geometries = restructureGeometry(svgGeom, svgSupportGeom, svgCutGeom);
    const model = renderTo3mf(geometries);

    const filename = `neon_${(new Date().toJSON().slice(0, 10))}_${Date.now()}.3mf`;
    saveFile(filename, model, /* type= */ 'application/3mf');
  }

  if (activity === IMPORT_BACKGROUND_BUTTON_SELECT) {
    const elem = window.document.createElement('input');
    elem.type = 'file';
    elem.accept = 'image/*';
    elem.addEventListener('change', () => {
      if (!elem.files?.[0]) {
        return;
      }
      const img = document.createElement('img');
      img.src = URL.createObjectURL(elem.files[0]);
      const canvas = document.getElementsByTagName('canvas')[0];
      canvas.style.backgroundSize = 'cover';
      canvas.style.backgroundImage = `url("${img.src}")`;
    });
    document.body.appendChild(elem);
    elem.click();
    document.body.removeChild(elem);
  }

  if (activity === CHANGE_PATH_TEXT) {
    const paths = getAllPaths();
    const path = paths.find((p) => p.id === args[1]);
    path.name = args[2];
  }

  if (activity === FOCUS_TAB) {
    const tab = args[1];
    const layer = getLayerByTab[tab]();
    layer.activate();
  }

  if (activity === SETTINGS_BUTTON_SELECT) {
    settingsState.setIsOpen(true);
  }
};

const centerEvent = (activity, args, tools, toolManager, gridDraw) => {
  const toolByActivity = {
    [TOOL_UNDO_SELECT]: tools.undo.pop.bind(tools.undo),
    [TOOL_ROTATE_LEFT]: tools.pathRotate.rotateLeft.bind(tools.pathRotate),
    [TOOL_ROTATE_RIGHT]: tools.pathRotate.rotateRight.bind(tools.pathRotate),
    [TOOL_MOVE_DOWN]: tools.pathMover.moveDown.bind(tools.pathMover),
    [TOOL_MOVE_LEFT]: tools.pathMover.moveLeft.bind(tools.pathMover),
    [TOOL_MOVE_UP]: tools.pathMover.moveUp.bind(tools.pathMover),
    [TOOL_MOVE_RIGHT]: tools.pathMover.moveRight.bind(tools.pathMover),
    [TOOL_MIRROR_H]: tools.pathMirror.mirrorHorizontal.bind(tools.pathMirror),
    [TOOL_MIRROR_V]: tools.pathMirror.mirrorVertical.bind(tools.pathMirror),
  };
  toolByActivity[activity]?.();

  if (activity === TOOL_CTRL_SELECT) {
    toolManager.setControlOverride(args[1]);
  }

  if (activity === TOOL_SHIFT_SELECT) {
    toolManager.setShiftOverride(args[1]);
  }

  if (activity === TOOL_SNAP_SELECT) {
    const isSnap2Grid = args[1];
    gridDraw.updateGrid(isSnap2Grid);
    toolManager.setSnap2Grid(isSnap2Grid);
  }
};

const modalEvent = (activity, args, settingsState) => {
  if (activity === SETTINGS_BUTTON_ACCEPT) {
    settingsState.setIsOpen(false);
  }

  if (activity === SETTINGS_ADJUSTED) {
    settingsState.setSetting(args[1], args[2]);
  }
};

export const onRenderWindows = (ui, update, tools, toolManager, gridDraw, settingsState) => {
  const eventListener = (id, ...args) => {
    if (id === LEFT_ID) {
      leftSideEvent(args[0], args);
    } else if (id === RIGHT_ID) {
      rightSideEvent(args[0], args, settingsState);
    } else if (id === CENTER_ID) {
      centerEvent(args[0], args, tools, toolManager, gridDraw);
    } else if (id === MODAL_ID) {
      modalEvent(args[0], args, settingsState);
    }

    update();
  };

  const safeEventListener = (id, ...args) => {
    try {
      eventListener(id, ...args);
    } catch (e) {
      console.error('Failure to handle a ui event. Woops! Hope the user can retry it.', e);
    }
  };

  ui.addEventListener(UI_EVENT.POINTER_UP, safeEventListener);
  ui.addEventListener(UI_EVENT.ON_CHANGE, safeEventListener);
};

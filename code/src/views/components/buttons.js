import { html } from 'htm/preact';
import { useState } from 'preact/hooks';
import Toastify from 'toastify-js';
import 'toastify-js/src/toastify.css';
import axios from 'axios';

import './buttons.css';

export const EXPORT_SVG_BUTTON_SELECT = 'export-svg-button-select';
export const EXPORT_3D_PRINT_BUTTON_SELECT = 'export-stl-button-select';
export const IMPORT_BACKGROUND_BUTTON_SELECT = 'import-background-button-select';
export const SETTINGS_BUTTON_SELECT = 'settings-button-select';

const doAPasteBoi = async (endpoint) => {
  const shorturlPromise = axios.get(endpoint).then(({ data }) => data);

  if (typeof ClipboardItem && navigator.clipboard.write) {
    let shorturl; // big ugly hack because safari doesnt allow async copy.
    // NOTE: Safari locks down the clipboard API to only work when triggered
    //   by a direct user interaction. You can't use it async in a promise.
    //   But! You can wrap the promise in a ClipboardItem, and give that to
    //   the clipboard API. Found this on https://developer.apple.com/forums/thread/691873
    // eslint-disable-next-line no-undef
    const text = new ClipboardItem({
      'text/plain':
        shorturlPromise
          .then((data) => { shorturl = data; return data; })
          .then((data) => new Blob([data], { type: 'text/plain' })),
    });
    return navigator.clipboard.write([text]).then(() => shorturl);
  }

  return shorturlPromise.then((data) => navigator.clipboard.writeText(data).then(() => data));
};

export const Buttons = ({ onClick }) => {
  const [isLoading, setLoading] = useState(false);
  const shorten = async (url) => {
    setLoading(true);

    // Alt https://cdpt.in/
    const endpoint = `https://tinyurl.com/api-create.php?url=${encodeURIComponent(url)}`;
    return doAPasteBoi(endpoint)
      .then((shorturl) => Toastify({
        text: `${shorturl}\nCopied to Clipboard`,
        duration: 10 * 1000,
        close: false,
        gravity: 'bottom', // `top` or `bottom`
        position: 'right', // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: {
          'text-align': 'center',
        },
      }).showToast())
      .catch(console.error)
      .finally(() => setLoading(false));
  };

  const [isPrinting, setPrinting] = useState(false);
  const print = async () => {
    setPrinting(true);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          onClick(EXPORT_3D_PRINT_BUTTON_SELECT);
          resolve();
        } catch (error) {
          reject(error);
        }
      }, 100);
    })
      .catch(console.error)
      .finally(() => setPrinting(false));
  };

  return html`
<div id='all-buttons'>
  <input type="button" disabled=${isLoading} value=${isLoading ? 'Loading...' : 'Share'} onClick=${() => shorten(window.location.href)} />
  <input type="button" disabled=${isPrinting} value=${isPrinting ? 'Loading...' : 'Export 3D Print'} onClick=${() => print()} />
  <input type="button" value="Import Background" onClick=${() => onClick(IMPORT_BACKGROUND_BUTTON_SELECT)} />
  <input type="button" value="Settings" onClick=${() => onClick(SETTINGS_BUTTON_SELECT)} />    
</div >`;
};

import { html } from 'htm/preact';
import { useEffect, useRef } from 'preact/hooks';

import { COLOR_VALUES } from './colors.js';
import { scalePixelsToMm } from '../../paper/scale-conversion';

import './paths.css';
import { preventBubble } from './inputs/stop-prop.js';

export const ADD_PATH_SELECT = 'add-path-select';
export const DUPLICATE_PATH_SELECT = 'duplicate-path-select';
export const REMOVE_PATH_SELECT = 'remove-path-select';
export const CHANGE_PATH_SELECT = 'change-path-select';
export const CHANGE_PATH_TEXT = 'change-path-text';
export const SMOOTH_PATH_SELECT = 'smooth-path-select';

export const FOCUS_TAB = 'tab-focus-clicked';
export const TAB_SUPPORTS = 'tab-supports';
export const TAB_PATHS = 'tab-paths';
export const TAB_CUTS = 'tab-cuts';

const PathMetadata = ({ selectedTab, length, strokeColor }) => {
  const nameByTab = {
    [TAB_PATHS]: 'path',
    [TAB_CUTS]: 'cut',
    [TAB_SUPPORTS]: 'support',
  };

  return html`<ul>
      <li title="This length should not be trusted.">Length: ${Math.floor(scalePixelsToMm(length) * 100) / 100}mm</li>
      <li>Color: ${COLOR_VALUES.find(({ hex }) => hex === strokeColor)?.name}</li>
      <li>Type: ${nameByTab[selectedTab]}</li>
  </ul>`;
};

const Path = ({
  name, strokeColor, selected, id, length,
  selectedTab, onChange, onClick,
}) => {
  const ref = useRef();

  useEffect(() => {
    if (!selected) {
      return;
    }

    const isElementInViewport = (el) => {
      const { scrollTop, clientHeight, offsetTop } = el.parentNode;
      const top = scrollTop + offsetTop;
      const bottom = scrollTop + offsetTop + clientHeight;

      // This isn't perfect, but I'm done messing with it.
      const rect = { top: el.offsetTop, bottom: el.offsetTop + el.clientHeight };
      return (rect.top >= top && rect.top < bottom) || (rect.bottom >= top && rect.bottom < bottom);
    };

    if (isElementInViewport(ref.current)) {
      return;
    }

    ref.current.scrollIntoView();
  }, [selected]);

  return html`
<div key=${id} ref=${ref} class='single-path ${selected ? 'path-selected' : ''}' onClick=${() => onClick(CHANGE_PATH_SELECT, id)}>
  <div class='single-path-input'>
    <div class='path-color-preview' style='background-color: ${strokeColor};' />
    <input type="text" value=${name} onChange=${(e) => preventBubble(e) && onChange(CHANGE_PATH_TEXT, id, e.target.value)}/>
    <input type="button" value="🗑️" title="delete" onClick=${(e) => preventBubble(e) && onClick(REMOVE_PATH_SELECT, id)} />
    <input type="button" value="📋" title="duplicate" onClick=${(e) => preventBubble(e) && onClick(DUPLICATE_PATH_SELECT, id)} />
  </div>
  <div>
    <${PathMetadata} selectedTab=${selectedTab} length=${length} strokeColor=${strokeColor} />
  </div>
</div>`;
};

const TabButton = ({
  onClick, selectedTab, tab, displayText,
}) => html`<button class="tablinks ${selectedTab === tab ? 'active' : ''}" onClick=${() => onClick(FOCUS_TAB, tab)}>${displayText}</button>`;

export const Paths = ({
  selectedTab, paths, onClick, onChange,
}) => {
  const tabInfo = {
    [TAB_PATHS]: { addtext: 'Add Path', displayText: 'Paths' },
    [TAB_SUPPORTS]: { addtext: 'Add Support', displayText: 'Supports' },
    [TAB_CUTS]: { addtext: 'Add Cut', displayText: 'Cuts' },
  };

  return html`
  <div id='all-paths-container'>
    <div class="tab">
      ${Object.entries(tabInfo).map(([tab, info]) => html`<${TabButton} selectedTab=${selectedTab} tab=${tab} displayText=${info.displayText} onClick=${onClick} />`)}
    </div>
    <input type='button' id='add-path-button' onClick=${() => onClick(ADD_PATH_SELECT)} value="${tabInfo[selectedTab]?.addtext} (${paths?.length || 0})" />
    <div id='all-paths'>
      ${paths?.map((path) => html`<${Path} key=${path.id} name=${path.name} strokeColor=${path.strokeColor.toCSS(true)} id=${path.id} 
                                          selected=${path.selected} length=${path.length} isSmoothed=${path.segments.find((segment) => segment.hasHandles())}
                                          selectedTab=${selectedTab} onChange=${onChange} onClick=${onClick} />`)}
    </div>
  </div >`;
};

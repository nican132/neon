/* eslint-disable object-curly-newline */
import { html } from 'htm/preact';

import white from './leds/white.png';
import naturalWhite from './leds/natural_white.png';
import warmWhite from './leds/warm_white.png';
import red from './leds/red.png';
import green from './leds/green.png';
import blue from './leds/blue.png';
import pink from './leds/pink.png';
import purple from './leds/purple.png';
import iceBlue from './leds/ice_blue.png';
import orange from './leds/orange.png';
import golden from './leds/golden.png';
import lemonYellow from './leds/lemon_yellow.png';

import './colors.css';

export const COLOR_VALUES = [
  { hex: '#ffffff', name: 'white', image: white },
  { hex: '#ffffdd', name: 'natural white', image: naturalWhite },
  { hex: '#fffeae', name: 'warm white', image: warmWhite },
  { hex: '#ffed79', name: 'lemon yellow', image: lemonYellow },
  { hex: '#fbb900', name: 'golden', image: golden },
  { hex: '#ff8f3d', name: 'orange', image: orange },
  { hex: '#dd0016', name: 'red', image: red },
  { hex: '#ff6ee3', name: 'pink', image: pink },
  { hex: '#c149f7', name: 'purple', image: purple },
  { hex: '#8098ff', name: 'blue', image: blue },
  { hex: '#00ffff', name: 'ice blue', image: iceBlue },
  { hex: '#00ffa6', name: 'green', image: green },
];

export const COLOR_SELECT = 'color-select-color';

const Color = ({
  hex, name, image, top, onClick,
}) => html`
  <div key=${hex} title=${name} class='single-color' onClick=${() => onClick(COLOR_SELECT, hex)}>
    <!--<span>${name}</span>-->
    <span class="color-preview-wrapper"><img class='color-preview-image' src=${image} style='top: ${top}' /></span>
    <span class='color-preview' style='background-color: ${hex};'  />
  </div>
  `;

export const Colors = ({ onClick }) => html`
  <div id='all-colors'>
    ${COLOR_VALUES.map(({ hex, name, image, top }) => html`<${Color} key=${hex} hex=${hex} name=${name} image=${image} top=${top} onClick=${onClick} />`)}
  </div > `;

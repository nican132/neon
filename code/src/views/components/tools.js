/* eslint-disable object-curly-newline */
import { html } from 'htm/preact';
import { useState, useRef, useEffect } from 'preact/hooks';
import { useKey } from 'rooks';

import './tools.css';

export const TOOL_UNDO_SELECT = 'tool-undo';
export const TOOL_SHIFT_SELECT = 'tool-shift';
export const TOOL_CTRL_SELECT = 'tool-ctrl';
export const TOOL_SNAP_SELECT = 'tool-snap';

export const TOOL_ROTATE_LEFT = 'tool-rotate-left';
export const TOOL_ROTATE_RIGHT = 'tool-rotate-right';
export const TOOL_MOVE_LEFT = 'tool-move-left';
export const TOOL_MOVE_RIGHT = 'tool-move-right';
export const TOOL_MOVE_UP = 'tool-move-up';
export const TOOL_MOVE_DOWN = 'tool-move-down';
export const TOOL_MIRROR_V = 'tool-mirror-v';
export const TOOL_MIRROR_H = 'tool-mirror-h';

const useKeyState = (keyCode) => {
  const [key, setKey] = useState(0);
  const keyListener = (event) => {
    setKey(event.type === 'keydown');
  };

  useKey(keyCode, keyListener, { eventTypes: ['keydown', 'keyup'] });

  return [key, (isPressed) => setKey(isPressed)];
};

const CheckboxKey = ({ onClick, keyCode }) => {
  const [isShift, setShift] = useKeyState(keyCode);
  const shiftRef = useRef();
  useEffect(() => onClick(isShift), [isShift]);

  return html`
    <div class="clickable">
      <input class="tool-checkbox" ref=${shiftRef} type="checkbox" checked=${isShift} id="${keyCode}" onClick=${() => setShift(shiftRef.current.checked)} />
      <label for="${keyCode}">${keyCode}</label>
    </div> 
  `;
};

const Checkbox = ({ onClick, text }) => {
  const [isChecked, setChecked] = useState(false);
  const checkRef = useRef();
  useEffect(() => onClick(isChecked), [isChecked]);

  return html`
    <div class="clickable">
      <input class="tool-checkbox" ref=${checkRef} type="checkbox" checked=${isChecked} id="${text}" onClick=${() => setChecked(checkRef.current.checked)} />
      <label for="${text}">${text}</label>
    </div> 
  `;
};

export const Tools = ({ onClick }) => html`
  <div id='all-tools'>
    <div class='tool-row'>
      <input class="tool-undo clickable" type="button" value="Undo" title="undo" onClick=${() => onClick(TOOL_UNDO_SELECT)} />
      <${CheckboxKey} keyCode="Shift" onClick=${(...args) => onClick(TOOL_SHIFT_SELECT, ...args)} />
      <${CheckboxKey} keyCode="Control" onClick=${(...args) => onClick(TOOL_CTRL_SELECT, ...args)} />
    </div>
    <div class='tool-row'>
      <${Checkbox} text="Snap2Grid" onClick=${(...args) => onClick(TOOL_SNAP_SELECT, ...args)} />
    </div>
    <div class='tool-row'>
      <div class='tool-sub-row'>
        <input class="tool-q clickable" type="button" value="⟲" title="rotate left" onClick=${() => onClick(TOOL_ROTATE_LEFT)} />
        <input class="tool-q clickable" type="button" value="↑" title="move up" onClick=${() => onClick(TOOL_MOVE_UP)} />
        <input class="tool-q clickable" type="button" value="⟳" title="rotate right" onClick=${() => onClick(TOOL_ROTATE_RIGHT)} />
        <input class="tool-q clickable" type="button" value="⇅" title="mirror vertically" onClick=${() => onClick(TOOL_MIRROR_V)} />
      </div>
      <div class='tool-sub-row'>
        <input class="tool-q clickable" type="button" value="←" title="move left" onClick=${() => onClick(TOOL_MOVE_LEFT)} />
        <input class="tool-q clickable" type="button" value="↓" title="move down" onClick=${() => onClick(TOOL_MOVE_DOWN)} />
        <input class="tool-q clickable" type="button" value="→" title="move right" onClick=${() => onClick(TOOL_MOVE_RIGHT)} />
        <input class="tool-q clickable" type="button" value="⮂" title="mirror horizontally" onClick=${() => onClick(TOOL_MIRROR_H)} />
      </div>
    </div>
  </div > `;

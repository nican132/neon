export const preventBubble = (e) => {
  e.stopPropagation();
  return e;
};

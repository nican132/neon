/* eslint-disable no-unused-vars */
/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable object-curly-newline */
import { html } from 'htm/preact';
import { toNumber } from 'lodash';
import { preventBubble } from './inputs/stop-prop.js';

import './settings.css';

export const SETTINGS_BUTTON_ACCEPT = 'settings-button-accept';
export const SETTINGS_ADJUSTED = 'settings-adjusted';

export const SETTINGS = {
  NEON_WIDTH_MM: 'settings-change-neon-width',
  NEON_CUT_LENGTH_MM: 'settings-change-neon-cut-length',
  WALL_WIDTH_MM: 'settings-change-wall-width',
  SHOW_SUPPORTS: 'settings-change-show-supports',
  SHOW_LIMITS: 'settings-change-show-limits',
  SHOW_CUTS: 'settings-change-show-cuts',
  SUPPORT_TYPE: 'settings-change-support-type',
};

export const SETTINGS_SUPPORT_TYPE = {
  FLAT_SUPPORTS: 'flat supports',
  FULL_SUPPORTS: 'full supports',
};

const NumericSetting = ({ settings, settingLabel, settingId, onChange, min = 0, max = 10 }) => html`
  <div class="single-setting-item">
    <label for=${settingId}>${settingLabel}</label>
    <input id=${settingId} type="number" value=${settings?.[settingId]} 
      step="0.1" min="${min}" max="${max}"
      onChange=${(e) => preventBubble(e) && onChange(SETTINGS_ADJUSTED, settingId, e.target.value)}/>
  </div>
`;

const TextEntrySetting = ({ settings, settingLabel, settingId, onChange }) => html`
  <div class="single-setting-item">
    <label for=${settingId}>${settingLabel}</label>
    <input id=${settingId} type="text" value=${settings?.[settingId]} onChange=${(e) => preventBubble(e) && onChange(SETTINGS_ADJUSTED, settingId, e.target.value)}/>
  </div>
`;

const CheckboxSetting = ({ settings, settingLabel, settingId, onChange }) => html`
  <div class="single-setting-item">
    <label for=${settingId}>${settingLabel}</label>
    <input id=${settingId} type="checkbox" checked=${settings?.[settingId]} onChange=${(e) => preventBubble(e) && onChange(SETTINGS_ADJUSTED, settingId, e.target.checked)}/>
  </div>
`;

const DropDownSetting = ({ settings, settingLabel, settingId, settingOptions, onChange }) => html`
  <div class="single-setting-item">
    <label for=${settingId}>${settingLabel}</label>
    <select id="${settingId}" onChange=${(e) => preventBubble(e) && onChange(SETTINGS_ADJUSTED, settingId, e.target.value)}>
      ${settingOptions.map((value) => html`
        <option selected=${settings?.[settingId] === value} value="${value}">${value}</option>
      `)}
    </select>
  </div>
`;

export const Settings = ({ settings, onClick, onChange }) => html`
    <div id='settings'>
      <div id='settings-items'>
        <h2>Neon Settings</h2>
        <${NumericSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.NEON_WIDTH_MM} settingLabel="Neon Width (mm): " />
        <${NumericSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.NEON_CUT_LENGTH_MM} settingLabel="Neon Cut Length (mm): " max="100" />
        <h2>Print Settings</h2>
        <${NumericSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.WALL_WIDTH_MM} settingLabel="Wall Width (mm): " />
        <${DropDownSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.SUPPORT_TYPE} 
          settingOptions=${Object.values(SETTINGS_SUPPORT_TYPE)} settingLabel="Print Support Type: " />
        <h2>Render Settings</h2>
        <${CheckboxSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.SHOW_LIMITS} settingLabel="Render Limits: " />
        <${CheckboxSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.SHOW_SUPPORTS} settingLabel="Render Supports: " />
        <${CheckboxSetting} onChange=${onChange} settings=${settings} settingId=${SETTINGS.SHOW_CUTS} settingLabel="Render Cuts: " />
      </div>
      <input type="button" value="Close" onClick=${() => onClick(SETTINGS_BUTTON_ACCEPT)} />    
    </div> 
`;

class SettingsState {
  #isSettingsOpen;
  #settings = {
    [SETTINGS.NEON_WIDTH_MM]: 5,
    [SETTINGS.NEON_CUT_LENGTH_MM]: 25.4,
    [SETTINGS.WALL_WIDTH_MM]: 1.5,
    [SETTINGS.SHOW_SUPPORTS]: true,
    [SETTINGS.SHOW_LIMITS]: true,
    [SETTINGS.SHOW_CUTS]: true,
    [SETTINGS.SUPPORT_TYPE]: SETTINGS_SUPPORT_TYPE.FULL_SUPPORTS,
  };

  #changeListener = () => { };

  setChangeListener(listener) {
    this.#changeListener = listener;
  }

  setIsOpen(isOpen) {
    this.#isSettingsOpen = isOpen;
    this.#changeListener();
  }

  setSetting(settingId, value) {
    const numericValue = toNumber(value);
    this.#settings[settingId] = Number.isNaN(numericValue) ? value : numericValue;
  }

  getSetting(settingId) {
    return this.#settings[settingId];
  }

  get() {
    return {
      isSettingsOpen: this.#isSettingsOpen,
      settings: this.#settings,
    };
  }
}

export const settingsState = new SettingsState();

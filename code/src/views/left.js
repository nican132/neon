import { html } from 'htm/preact';
import { useEffect, useState } from 'preact/hooks';

import { Colors } from './components/colors.js';

export const LEFT_ID = 'left-id';

const LeftBody = ({ onClick }) => html`<${Colors} onClick=${onClick} />`;

export const Left = ({ onClick, stateSubscriber }) => {
  const [, setState] = useState({});
  useEffect(() => {
    stateSubscriber?.(LEFT_ID, setState);
  }, [stateSubscriber, setState]);

  return html`<${LeftBody} onClick=${(...args) => onClick(LEFT_ID, ...args)} />`;
};

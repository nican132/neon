import { render } from 'preact';
import { html } from 'htm/preact';
import pWaitFor from 'p-wait-for';

import { Left } from './left.js';
import { Right } from './right.js';
import { Center } from './center.js';
import { CenterArrow } from './center-arrow';
import { Modal } from './modal.js';

import './ui.css';

const RIGHT_SIDE_UI_ID = 'right-side-ui';
const LEFT_SIDE_UI_ID = 'left-side-ui';
const CENTER_UI_ID = 'center-ui';
const CENTER_ARROW_UI_ID = 'center-arrow-container';
const MODAL_UI_ID = 'modal-ui';

const renderables = [
  { renderable: Left, container: LEFT_SIDE_UI_ID },
  { renderable: Center, container: CENTER_UI_ID },
  { renderable: CenterArrow, container: CENTER_ARROW_UI_ID },
  { renderable: Right, container: RIGHT_SIDE_UI_ID },
  { renderable: Modal, container: MODAL_UI_ID },
];

export const UI_EVENT = {
  // User clicks something.
  POINTER_UP: 'ui_pick', // Args will be whatever the particular implementation window supplies.

  // Similar for change.
  ON_CHANGE: 'on_change',
};

const generateEmptyEventListeners = () => Object.values(UI_EVENT).reduce((acc, ev) => ({ ...acc, [ev]: [] }), {});

export class Ui {
  #eventListeners = generateEmptyEventListeners();
  #stateSubscribers = {};

  /** Sets to the callback will be triggered upon the specified events. */
  addEventListener = (event, callback) => {
    this.#eventListeners[event].push(callback);
  };

  removeAllEventListeners = () => {
    this.#eventListeners = generateEmptyEventListeners;
  };

  /** Updates the state of one of the windows. */
  updateState = (identifier, state) => {
    if (Object.keys(this.#stateSubscribers).length === 0) {
      // Assume we are still loading...
      return;
    }

    if (!this.#stateSubscribers[identifier]) {
      throw new Error(`Can't update ${identifier}, must be one of ${Object.keys(this.#stateSubscribers)}.`);
    }

    this.#stateSubscribers[identifier](state);
  };

  renderWindows = async () => {
    const onChange = (...args) => this.#eventListeners[UI_EVENT.ON_CHANGE].forEach((listener) => listener(...args));
    const onClick = (...args) => this.#eventListeners[UI_EVENT.POINTER_UP].forEach((listener) => listener(...args));
    const stateSubscriber = (identifier, setState) => { this.#stateSubscribers[identifier] = setState; };

    renderables.forEach(({ renderable, container }) => render(
      html`<${renderable} onClick=${onClick} onChange=${onChange} stateSubscriber=${stateSubscriber} />`,
      document.getElementById(container),
    ));

    return pWaitFor(() => Object.keys(this.#stateSubscribers).length === renderables.length, { timeout: 1000 });
  };
}

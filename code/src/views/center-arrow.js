import { html } from 'htm/preact';
import { useEffect, useState } from 'preact/hooks';
import { CANVAS_WIDTH_IN_MM } from '../paper/scale-conversion';

export const CENTER_ARROW_ID = 'center-arrow-id';

const Body = ({ zoom }) => html`<div class="center-arrow" id="center-arrow">
  <div>←</div>
  <div>${Number(CANVAS_WIDTH_IN_MM / zoom).toFixed(2)}mm (${Number(zoom * 100).toFixed(0)}%)</div>
  <div>→</div>
</div>`;

export const CenterArrow = ({ stateSubscriber }) => {
  const [state, setState] = useState({});
  useEffect(() => {
    stateSubscriber?.(CENTER_ARROW_ID, setState);
  }, [stateSubscriber, setState]);

  return html`<${Body} zoom=${state.zoom || 1.0} />`;
};

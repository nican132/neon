import { memoize } from 'lodash';

export const NEON_BEND_RADIUS_IN_MM = 10; // Picked arbitrarily, seems good though.
export const NEON_HEIGHT_IN_MM = 8.5; // This is just the non-visible height of the neon.
export const TRACK_SLOT_WIDTH_IN_MM = 2.5; // This is the slot on the bottom that wires pass through.
export const TRACK_WIRE_ROUTE_HEIGHT_IN_MM = 3.25; // This is the height for wires beneath the neon.
export const CANVAS_WIDTH_IN_MM = 800; // almost 2 feet

const getCanvas = memoize(() => document.getElementsByTagName('canvas')[0]);

export const getCanvasWidth = memoize(() => parseFloat(getCanvas().scrollWidth)); // style.width has a known bug of being NaN.

export const scaleMmToPixels = (mm, width = getCanvasWidth()) => (mm * width) / CANVAS_WIDTH_IN_MM;

export const scalePixelsToMm = (pixels, width = getCanvasWidth()) => (pixels * CANVAS_WIDTH_IN_MM) / width;

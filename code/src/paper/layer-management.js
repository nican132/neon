import paper, { Layer } from 'paper';

import { TAB_CUTS, TAB_PATHS, TAB_SUPPORTS } from '../views/components/paths.js';

export const getOrCreateLayer = (name) => paper.project.layers.find((l) => l.name === name)
  || paper.project.addLayer(new Layer({ name }));

export const interactiveLayerName = 'main-interactive-layer';
export const supportLayerName = 'main-support-layer';
export const cutLayerName = 'main-cut-layer';

export const getInteractiveLayer = () => getOrCreateLayer(interactiveLayerName);
export const getSupportLayer = () => getOrCreateLayer(supportLayerName);
export const getCutLayer = () => getOrCreateLayer(cutLayerName);
export const getInteractiveProcessLayer = () => getOrCreateLayer('post-processed');
export const getSupportProcessLayer = () => getOrCreateLayer('post-processed-supports');
export const getCutProcessLayer = () => getOrCreateLayer('post-processed-cuts');

export const tabByLayerName = {
  [interactiveLayerName]: TAB_PATHS,
  [supportLayerName]: TAB_SUPPORTS,
  [cutLayerName]: TAB_CUTS,
};

export const getLayerByTab = {
  [TAB_PATHS]: getInteractiveLayer,
  [TAB_SUPPORTS]: getSupportLayer,
  [TAB_CUTS]: getCutLayer,
};

/* eslint-disable consistent-return */
import paper, { Path, Point } from 'paper';

export const getAllPaths = () => paper.project.activeLayer.getItems({ className: 'Path' });

export const getSelectedPath = () => paper.project.activeLayer.getItems({ selected: true, className: 'Path' }).at(0);

export const getSelectedPoint = () => {
  const selectedPath = getSelectedPath();
  if (!selectedPath) {
    return;
  }

  const selectedSegment = selectedPath.segments.find((segment) => segment.selected);
  if (!selectedSegment) {
    return;
  }

  const selectedPoint = selectedSegment.point;
  if (!selectedPoint.selected) {
    return;
  }

  return {
    path: selectedPath,
    segment: selectedSegment,
    point: selectedPoint,
  };
};

export const getSelectedCenter = () => {
  const path = getSelectedPath();
  if (!path) {
    return;
  }

  const center = path.segments.map((segment) => segment.point).reduce((acc, cur) => acc.add(cur), new Point()).divide(path.segments.length);

  return { center, path };
};

/** Returns a selected path item that passes the hit test. If a segment also passes, then the segment field will be set. */
export const hitTestPoint = (point) => {
  const path = getSelectedPath();
  if (!path) {
    return;
  }

  const hitOptions = {
    class: Path,
    fill: true,
    stroke: true,
    segments: true,
    curves: true,
    tolerance: 10,
  };
  return path.hitTest(point, hitOptions);
};

/* eslint-disable no-param-reassign */
import paper from 'paper';
import JSONCrush from 'jsoncrush';
import LZUTF8 from 'lzutf8';

import { getCutLayer, getInteractiveLayer, getSupportLayer } from './layer-management.js';
import { CANVAS_WIDTH_IN_MM, getCanvasWidth } from './scale-conversion.js';
import { scaleRadius } from '../tools/point-radius.js';

const importJsonLayer = (layer, loadedWidth, loadedWidthMm, paperImport) => {
  try {
    layer.importJSON(paperImport);

    const canvasWidth = getCanvasWidth();
    const scaleFacter = canvasWidth / loadedWidth;

    layer.scale(scaleFacter, new paper.Point(0, 0));
    // Don't need to scaleRadius here.

    const canvasWidthMm = CANVAS_WIDTH_IN_MM;
    const scaleFactorMm = loadedWidthMm / canvasWidthMm;

    layer.scale(scaleFactorMm, new paper.Point(0, 0));
    layer.getItems({ className: 'Path' }).forEach((path) => scaleRadius(path, scaleFactorMm));
  } catch (e) {
    console.error(`Couldn't import json: ${paperImport}`, e);
  }
};

export const loadFromUrl = () => {
  if (!window.location.hash) {
    return;
  }

  // Previous iterations of this tool used JSONCrush and wasn't Base64 encoded and
  // a bunch of other things. These strange things stick around to be backwards compatible.
  const oldDefaultWidthMm = 560;

  try {
    const uriHash = decodeURIComponent(window.location.hash.split('#')[1]);
    const jsonString = uriHash.startsWith('{') ? uriHash : JSONCrush.uncrush(uriHash);
    const json = JSON.parse(jsonString);

    const paperImport = json.comp ? LZUTF8.decompress(json.paper, { inputEncoding: 'Base64' }) : JSON.stringify(json.paper);
    importJsonLayer(getInteractiveLayer(), json.width, json.widthMm || oldDefaultWidthMm, paperImport);

    if (json.support) {
      const supportImport = LZUTF8.decompress(json.support, { inputEncoding: 'Base64' });
      importJsonLayer(getSupportLayer(), json.width, json.widthMm, supportImport);
    }

    if (json.cut) {
      const cutImport = LZUTF8.decompress(json.cut, { inputEncoding: 'Base64' });
      importJsonLayer(getCutLayer(), json.width, json.widthMm, cutImport);
    }
  } catch (e) {
    console.error(`Couldn't parse json: ${window.location.hash}`, e);
  }
};

const compressLayer = (layer) => {
  const previousVisible = layer.visible;
  layer.visible = true;
  const json = layer.exportJSON({ asString: true, precision: 3 });
  layer.visible = previousVisible;
  return LZUTF8.compress(json, { outputEncoding: 'Base64' });
};

const generateUrlHash = () => {
  const json = JSON.stringify({
    width: getCanvasWidth(),
    widthMm: CANVAS_WIDTH_IN_MM,
    comp: true, // Is it compressed
    paper: compressLayer(getInteractiveLayer()),
    support: compressLayer(getSupportLayer()),
    cut: compressLayer(getCutLayer()),
  });

  // Thanks https://stackoverflow.com/questions/44429173/javascript-encodeuri-failed-to-encode-round-bracket
  return encodeURIComponent(json).replace(/[!'()*]/g, (c) => `%${c.charCodeAt(0).toString(16)}`);
};

export const saveToUrl = () => {
  window.location.hash = generateUrlHash();
};

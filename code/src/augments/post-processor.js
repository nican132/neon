/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable no-param-reassign */
import paper from 'paper';
import { minBy } from 'lodash';

import { makePath } from '../factory/path-factory.js';

import { getRadiusInMm } from '../tools/point-radius.js';

import { NEON_BEND_RADIUS_IN_MM, scaleMmToPixels } from '../paper/scale-conversion.js';
import {
  cutLayerName,
  getCutLayer,
  getCutProcessLayer,
  getInteractiveLayer, getInteractiveProcessLayer, getSupportLayer, getSupportProcessLayer, supportLayerName,
} from '../paper/layer-management.js';

import { settingsState, SETTINGS } from '../views/components/settings.js';

// Note that this is an imperfect clone.
const clonePathProperties = (pathInput) => {
  const path = makePath();
  path.selected = false;
  path.strokeColor = pathInput.strokeColor.clone(); // Can't use path.strokeColor = pathInput.strokeColor for some reason?
  path.strokeWidth = scaleMmToPixels(settingsState.getSetting(SETTINGS.NEON_WIDTH_MM));
  path.addSegments(pathInput.segments.map((segment) => segment.point.clone()));
  path.neon.parent = pathInput; // This is our own custom field!!

  return path;
};

// This mutates the path, just fyi.
const buildSnapPoints = (path, snappedLayer) => {
  const snapThresholdPixels = scaleMmToPixels(5);

  const snapPoints = path.segments
    .map((segment) => segment.point)
    .map((point) => {
      const snappedLocations = snappedLayer.getItems({ className: 'Path' })
        .map((snappedPath) => {
          const distToFirstSegment = snappedPath.firstSegment?.point.getDistance(point) || snapThresholdPixels + 10;
          const distToLastSegment = snappedPath.lastSegment?.point.getDistance(point) || snapThresholdPixels + 10;
          const offset = scaleMmToPixels(1);
          if (distToFirstSegment < distToLastSegment && distToFirstSegment < snapThresholdPixels) {
            return snappedPath.getNearestLocation(snappedPath.getPointAt(offset));
          }

          if (distToLastSegment < distToFirstSegment && distToLastSegment < snapThresholdPixels) {
            return snappedPath.getNearestLocation(snappedPath.getPointAt(snappedPath.length - offset));
          }

          return snappedPath.getNearestLocation(point);
        }).filter((snappedLocation) => snappedLocation); // Remove nulls that show for some reason.

      if (!snappedLocations || snappedLocations.length === 0) {
        return point.clone();
      }

      const nearestSnappedLocation = minBy(snappedLocations, (snappedLocation) => snappedLocation.distance);
      if (nearestSnappedLocation.distance <= snapThresholdPixels) {
        return nearestSnappedLocation.point.clone();
      }

      return point.clone();
    });

  path.removeSegments();
  path.addSegments(snapPoints);
  return path;
};

// Thanks https://gist.github.com/winduptoy/8b5c574e0e33bf547a31
// This mutates the path, just fyi.
const roundPath = (path) => {
  const minRadiusMm = NEON_BEND_RADIUS_IN_MM;
  const roundedSegments = path.segments
    .flatMap((segment) => {
      if (segment.isFirst() || segment.isLast()) {
        return [segment.point.clone()];
      }

      const localRadiusInMM = getRadiusInMm(path.neon.parent, segment.index);
      const radius = scaleMmToPixels(localRadiusInMM || minRadiusMm);

      const curPoint = segment.point;
      const nextPoint = segment.next.point;
      const prevPoint = segment.previous.point;
      const nextDelta = curPoint.subtract(nextPoint);
      const prevDelta = curPoint.subtract(prevPoint);
      nextDelta.length = radius;
      prevDelta.length = radius;
      return [{
        point: curPoint.subtract(prevDelta),
        handleOut: prevDelta.divide(2.0),
      }, {
        point: curPoint.subtract(nextDelta),
        handleIn: nextDelta.divide(2.0),
      }];
    });

  path.removeSegments();
  path.addSegments(roundedSegments);
  return path;
};

export class PostProcessor {
  #processedPathsById = {}; // Simple cache layer.

  process() {
    const drawLayer = paper.project.activeLayer;

    const layersToProcess = [{
      layer: getInteractiveLayer(),
      processLayer: getInteractiveProcessLayer(),
      isVisible: true,
      snappedLayer: undefined,
      isRounded: true,
    }, {
      layer: getSupportLayer(),
      processLayer: getSupportProcessLayer(),
      isVisible: settingsState.getSetting(SETTINGS.SHOW_SUPPORTS),
      isRounded: false,
      snappedLayer: getInteractiveProcessLayer(),
    }, {
      layer: getCutLayer(),
      processLayer: getCutProcessLayer(),
      isVisible: settingsState.getSetting(SETTINGS.SHOW_CUTS),
      isRounded: false,
      snappedLayer: undefined,
    }];

    layersToProcess.forEach(({
      layer, processLayer, isVisible, snappedLayer, isRounded,
    }) => {
      const extraPathProperties = {
        dashArray: layer.name === supportLayerName ? [10, 4] : undefined,
        strokeWidth: layer.name === cutLayerName ? 1 : undefined,
      };
      const opacity = layer.name === drawLayer.name ? 1.0 : 0.5;
      processLayer.opacity = opacity;

      this.#processSingleLayer(layer.getItems({ className: 'Path' }), processLayer, extraPathProperties, { isVisible, snappedLayer, isRounded });
    });

    drawLayer.activate();
  }

  #processSingleLayer(paths, processLayer, extraPathProperties, extraOperations) {
    processLayer.activate();
    processLayer.removeChildren();

    if (!extraOperations.isVisible) {
      return;
    }

    const isCached = (path) => !path.selected && this.#processedPathsById[processLayer.id]?.[path.id];
    const cachedPaths = paths
      .filter(isCached)
      .map((path) => this.#processedPathsById[processLayer.id][path.id]);
    const changedPaths = paths
      .filter((path) => !isCached(path))
      .filter((path) => path.segments.length > 1)
      .map((path) => clonePathProperties(path)) // Side effect adds to the processLayer.
      .map((path) => (extraOperations.snappedLayer ? buildSnapPoints(path, extraOperations.snappedLayer) : path))
      .map((path) => (extraOperations.isRounded ? roundPath(path) : path))
      .map((path) => {
        Object.entries(extraPathProperties).filter(([key, value]) => key && value).forEach(([key, value]) => {
          path[key] = value;
        });
        return path;
      });

    // cachedPaths need re-added to processLayer, changedPaths were already added by side effect.
    processLayer.addChildren(cachedPaths);

    this.#processedPathsById[processLayer.id] = [...cachedPaths, ...changedPaths].reduce((acc, cur) => ({ ...acc, [cur.neon.parent.id]: cur }), {});
  }

  clearCache() {
    this.#processedPathsById = {};
  }
}

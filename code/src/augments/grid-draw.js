/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import paper, { Path, Point } from 'paper';

import { getCanvasWidth, scaleMmToPixels } from '../paper/scale-conversion.js';

import { getOrCreateLayer } from '../paper/layer-management.js';
import { SETTINGS, settingsState } from '../views/components/settings.js';

export const getGridSizeInPixels = () => scaleMmToPixels(settingsState.getSetting(SETTINGS.NEON_CUT_LENGTH_MM));

export const getNearestGridPixel = (coord, gridSize) => {
  const gridSizeInPixels = gridSize || getGridSizeInPixels(); // Can't be a default param because that would call scale before we have a canvas.
  return Math.round(coord / gridSizeInPixels) * gridSizeInPixels;
};

export class GridDraw {
  #isDrawn;

  get name() {
    return 'grid-draw';
  }

  getGridLayer() {
    return getOrCreateLayer(this.name);
  }

  updateGrid(isDrawn) {
    this.#isDrawn = isDrawn;
  }

  draw() {
    const drawLayer = paper.project.activeLayer;

    const processLayer = this.getGridLayer(this.name);
    processLayer.activate();
    processLayer.removeChildren();

    if (this.#isDrawn) {
      const gridSizeInPixels = getGridSizeInPixels();
      const maxX = Math.ceil(getCanvasWidth() / gridSizeInPixels);

      for (let x = 0; x <= maxX; x += 1) {
        for (let y = 0; y <= maxX; y += 1) {
          const arc = new Path.RegularPolygon(new Point(x * gridSizeInPixels, y * gridSizeInPixels), /* sides= */ 4, /* radius= */ scaleMmToPixels(1));
          arc.fillColor = '#553333';
        }
      }
    }
    processLayer.sendToBack();

    drawLayer.activate();
  }
}

import svgDeserializer from '@jscad/svg-deserializer';
import { CANVAS_WIDTH_IN_MM, getCanvasWidth } from '../paper/scale-conversion';

/**
 * Converts an SVG String to jscad geometry.
 */
export const deserializeToGeometry = (rawData) => svgDeserializer.deserialize({ filename: 'file.svg', output: 'geometry' }, rawData);

/**
 * Converts paperjs array of paths to SVG String.
 */
export const serializeToString = (paths) => {
  const svgPaths = paths.map((path) => path.exportSVG());

  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.setAttribute('style', 'background-color: black;');
  svg.setAttribute('viewBox', `0 0 ${getCanvasWidth()} ${getCanvasWidth()}`);
  svg.setAttribute('width', `${CANVAS_WIDTH_IN_MM}mm`);
  svg.setAttribute('height', `${CANVAS_WIDTH_IN_MM}mm`);
  svg.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', 'http://www.w3.org/1999/xlink');
  svgPaths.forEach((svgPath) => svg.appendChild(svgPath));

  return new XMLSerializer().serializeToString(svg);
};

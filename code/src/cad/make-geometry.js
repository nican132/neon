/* eslint-disable consistent-return */
import { zip } from 'lodash';
import {
  expansions, extrusions, booleans, transforms, primitives,
} from '@jscad/modeling';

import { NEON_HEIGHT_IN_MM, TRACK_SLOT_WIDTH_IN_MM, TRACK_WIRE_ROUTE_HEIGHT_IN_MM } from '../paper/scale-conversion.js';

import { SETTINGS, settingsState, SETTINGS_SUPPORT_TYPE } from '../views/components/settings.js';

const { line } = primitives;
const { expand } = expansions;
const { extrudeLinear } = extrusions;
const { subtract, union, scission } = booleans;
const { translateZ } = transforms;

const corners = 'edge'; // or 'round' or 'chamfer' or 'edge'
const expandStrip = (pathway, delta) => expand({ delta, corners }, pathway);
const extrudeShape = (stripway, height) => extrudeLinear({ height }, stripway);
const translateShape = (shape, amount) => translateZ(amount, shape);
const unionAll = (acc, cur) => (acc ? union(acc, cur) : cur);

const getNeonWidthMm = () => settingsState.getSetting(SETTINGS.NEON_WIDTH_MM);
const getWallWidthMm = () => settingsState.getSetting(SETTINGS.WALL_WIDTH_MM);

const getInnerDelta = () => getNeonWidthMm() / 2.0; // Delta means half the width of the neon.
const getOuterDelta = () => getInnerDelta() + getWallWidthMm(); // Delta half the width the neon + walls.

const buildUpper = (svgGeom) => {
  const neon = svgGeom
    .map((pathway) => expandStrip(pathway, getInnerDelta()))
    .map((stripway) => extrudeShape(stripway, NEON_HEIGHT_IN_MM))
    .map((shape) => translateShape(shape, getWallWidthMm()));

  const neonTrack = svgGeom
    .map((pathway) => expandStrip(pathway, getOuterDelta()))
    .map((stripway) => extrudeShape(stripway, NEON_HEIGHT_IN_MM + getWallWidthMm()));

  return zip(neonTrack, neon).map((shapes) => subtract(...shapes)).reduce(unionAll);
};

const buildLower = (svgGeom, svgSupportGeom) => {
  const slotDelta = TRACK_SLOT_WIDTH_IN_MM / 2.0; // Delta means this will be multiplied by 2 when fully expanded.
  const undersideGeom = [...svgGeom, ...svgSupportGeom];

  if (settingsState.getSetting(SETTINGS.SUPPORT_TYPE) === SETTINGS_SUPPORT_TYPE.FLAT_SUPPORTS) {
    return undersideGeom
      .map((pathway) => expandStrip(pathway, getOuterDelta()))
      .map((stripway) => extrudeShape(stripway, getWallWidthMm()));
  }

  const buildThroughhole = (pointA, pointB) => {
    const vector = [pointA[0] - pointB[0], pointA[1] - pointB[1]];
    const length = Math.sqrt((vector[0] * vector[0]) + (vector[1] * vector[1]));
    const normalizedVector = [vector[0] / length, vector[1] / length];
    const point = [pointA[0] + normalizedVector[0] * slotDelta * 2, pointA[1] + normalizedVector[1] * slotDelta * 2];
    const path = line([pointA, point]);
    const strip = expandStrip(path, getInnerDelta());
    return extrudeShape(strip, getWallWidthMm());
  };

  const throughholes = svgGeom
    .flatMap((pathway) => [
      [pathway.points[0], pathway.points[1]],
      [pathway.points[pathway.points.length - 1], pathway.points[pathway.points.length - 2]],
    ]).map((points) => buildThroughhole(...points));

  const wire = undersideGeom
    .map((pathway) => expandStrip(pathway, getInnerDelta()))
    .map((stripway) => extrudeShape(stripway, TRACK_WIRE_ROUTE_HEIGHT_IN_MM))
    .map((shape) => translateShape(shape, -TRACK_WIRE_ROUTE_HEIGHT_IN_MM));

  const wireSlotHeight = TRACK_WIRE_ROUTE_HEIGHT_IN_MM + getWallWidthMm();
  const wireSlot = undersideGeom
    .map((pathway) => expandStrip(pathway, slotDelta))
    .map((stripway) => extrudeShape(stripway, wireSlotHeight))
    .map((shape) => translateShape(shape, -wireSlotHeight));

  const wireTrackHeight = TRACK_WIRE_ROUTE_HEIGHT_IN_MM + (getWallWidthMm() * 2);
  const wireTrack = undersideGeom
    .map((pathway) => expandStrip(pathway, getOuterDelta()))
    .map((stripway) => extrudeShape(stripway, wireTrackHeight))
    .map((shape) => translateShape(shape, -wireTrackHeight + getWallWidthMm()));

  const allWire = zip(wire, wireSlot, throughholes)
    .map((shapes) => shapes.filter((shape) => shape))
    .map((shapes) => union(...shapes))
    .reduce(unionAll);
  const allWireTrack = wireTrack.reduce(unionAll);

  return subtract(allWireTrack, allWire);
};

const buildCut = (structure, svgCutGeom) => {
  if (svgCutGeom.length === 0) {
    return [structure];
  }

  const cuts = svgCutGeom
    .map((pathway) => expandStrip(pathway, 0.01))
    .map((stripway) => extrudeShape(stripway, NEON_HEIGHT_IN_MM * 4))
    .map((shape) => translateShape(shape, -NEON_HEIGHT_IN_MM * 2))
    .reduce(unionAll);

  return scission(subtract(structure, cuts));
};

/**
 * Convert a 2d jscad geometry (from svg) to 3D.
 * It should already be scaled to mm.
 */
export const restructureGeometry = (svgGeom, svgSupportGeom, svgCutGeom) => {
  const upper = buildUpper(svgGeom);
  const lower = buildLower(svgGeom, svgSupportGeom);
  return buildCut(union(upper, lower), svgCutGeom);
};

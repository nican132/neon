import threemfSerializer from '@jscad/3mf-serializer';

/**
 * Converts jscad geometry to 3MF.
 */
export const renderTo3mf = (geometries) => threemfSerializer.serialize({ unit: 'millimeter' }, ...geometries)[0];

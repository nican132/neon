/* eslint-disable no-param-reassign */
import paper from 'paper';
import { isEqual } from 'lodash';

import { PathExtender } from './tools/path-extender.js';
import { PointAdder } from './tools/point-adder.js';
import { PointRemover } from './tools/point-remover.js';
import { ToolManager } from './tools/tool-manager.js';
import { Undo } from './tools/undo.js';
import { PathUnsmooth } from './tools/path-unsmooth.js';
import { PathScaler } from './tools/path-scaler.js';
import { PathMover } from './tools/path-mover.js';
import { PointMover } from './tools/point-mover.js';
import { Click } from './tools/click.js';
import { PointRadius } from './tools/point-radius.js';
import { PathMirror } from './tools/path-mirror.js';
import { PathRotate } from './tools/path-rotate.js';
import { CopyPaste } from './tools/copy-paste.js';
import { PointUnselect } from './tools/point-unselect.js';
import { CameraMovement } from './tools/camera-movement.js';

import { Ui } from './views/ui.js';
import { RIGHT_ID } from './views/right.js';
import { onRenderWindows } from './views/ui-events.js';
import { MODAL_ID } from './views/modal.js';
import { CENTER_ARROW_ID } from './views/center-arrow.js';
import { SETTINGS, settingsState } from './views/components/settings.js';

import { Limiter } from './limits/limiter.js';
import { PathAngle } from './limits/path-angle.js';
import { PathLength } from './limits/path-length.js';
import { PathIntersection } from './limits/path-intersection.js';
import { PointBendIntersection } from './limits/point-bend-intersection.js';
import { PointSingle } from './limits/point-single.js';

import { makePath } from './factory/path-factory.js';

import { PostProcessor } from './augments/post-processor.js';
import { GridDraw } from './augments/grid-draw.js';

import {
  getCutLayer,
  getInteractiveLayer, getInteractiveProcessLayer,
  getSupportLayer, tabByLayerName,
} from './paper/layer-management.js';
import { getAllPaths } from './paper/get-selected-item.js';
import { loadFromUrl, saveToUrl } from './paper/save-load.js';

const init = () => {
  const canvas = document.getElementsByTagName('canvas')[0];
  paper.setup(canvas);

  [getCutLayer(), getSupportLayer(), getInteractiveLayer()].forEach((layer) => {
    layer.activate();
    makePath();
  });

  loadFromUrl();

  console.log(`Loaded in draw paths: ${getInteractiveLayer().exportJSON()}`);

  const pointSingle = new PointSingle();
  const pathIntersection = new PathIntersection();
  const pointBendIntersection = new PointBendIntersection();
  const pathLength = new PathLength();
  const pointAngle = new PathAngle();
  const limiter = new Limiter([pointAngle, pathLength, pathIntersection, pointBendIntersection, pointSingle]);

  const cameraMovement = new CameraMovement();
  const copyPaste = new CopyPaste();
  const pathRotate = new PathRotate();
  const pathMirror = new PathMirror();
  const pointRadius = new PointRadius();
  const pathScaler = new PathScaler();
  const pathUnsmooth = new PathUnsmooth();
  const url = new Click();
  const click = new Click();
  const undo = new Undo();
  const pointRemover = new PointRemover();
  const pointMover = new PointMover();
  const pathMover = new PathMover();
  const pointAdder = new PointAdder();
  const pathExtender = new PathExtender();
  const pointUnselect = new PointUnselect();
  const toolManager = new ToolManager([
    cameraMovement,
    copyPaste, pathMirror, pathRotate, pathUnsmooth,
    pathScaler, pointRemover, pathMover, pointRadius,
    pointMover, pointAdder, pathExtender, pointUnselect,
    undo, click, url]); // These have a magic order.

  const gridDraw = new GridDraw();
  const postProcessor = new PostProcessor();

  undo.store();
  url.setClickListener(saveToUrl);

  const ui = new Ui();
  ui.renderWindows().then(() => {
    const updateSettingsUiState = () => {
      ui.updateState(MODAL_ID, settingsState.get());
    };

    settingsState.setChangeListener(() => {
      updateSettingsUiState();
      postProcessor.clearCache();
      postProcessor.process();
      getSupportLayer().visible = settingsState.getSetting(SETTINGS.SHOW_SUPPORTS);
      getCutLayer().visible = settingsState.getSetting(SETTINGS.SHOW_CUTS);
      limiter.clearConflicts();
      limiter.checkLimits(getInteractiveLayer(), getInteractiveProcessLayer());
    });

    const updateRightUiState = () => {
      postProcessor.process();
      ui.updateState(RIGHT_ID, {
        selectedTab: tabByLayerName[paper.project.activeLayer.name],
        paths: getAllPaths(),
      });
    };

    undo.setPopListener(updateRightUiState);
    click.setClickListener(updateRightUiState);

    onRenderWindows(ui, () => {
      undo.store();
      url.onMouseUp();
      updateRightUiState();
    }, {
      undo, pathMover, pathMirror, pathRotate,
    }, toolManager, gridDraw, settingsState);

    updateRightUiState();
    saveToUrl();
  });

  let old = {};
  paper.view.onFrame = () => {
    const current = {
      interactive: getInteractiveLayer().exportJSON(),
      support: getSupportLayer().exportJSON(),
      cut: getCutLayer().exportJSON(),
      zoom: paper.view.zoom,
    };
    if (!isEqual(old, current)) {
      old = current;
      postProcessor.process();
      limiter.checkLimits(getInteractiveLayer(), getInteractiveProcessLayer());
      ui.updateState(CENTER_ARROW_ID, { zoom: paper.view.zoom });
    }

    gridDraw.draw();
  };
};

window.addEventListener('load', () => init());

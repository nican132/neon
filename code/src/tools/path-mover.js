/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { Point } from 'paper';

import { scaleMmToPixels } from '../paper/scale-conversion.js';
import { getSelectedPath, hitTestPoint } from '../paper/get-selected-item.js';

const AMOUNT_IN_PIXELS = scaleMmToPixels(1);

/**
 * Class is a tool that lets you click and drag segment points.
 */
export class PathMover {
  #moving;

  onMouseMove(event, overrides, snapEvent) {
    if (!this.#moving) {
      return;
    }

    const selectedPath = getSelectedPath();
    if (!selectedPath) {
      return;
    }

    selectedPath.position = selectedPath.position.add(overrides.snap2Grid ? snapEvent.deltaSnapPoint : event.delta);
    return true;
  }

  onMouseDown(event, overrides) {
    if (!event.event.shiftKey && !overrides.shiftKey) {
      return;
    }

    const hitResult = hitTestPoint(event.point);
    if (!hitResult || hitResult.segment) {
      return;
    }

    this.#moving = true;
    return true;
  }

  onMouseUp() {
    this.#moving = false;
  }

  onKeyDown(event) {
    const actionByKey = {
      a: this.moveLeft.bind(this),
      w: this.moveUp.bind(this),
      s: this.moveDown.bind(this),
      d: this.moveRight.bind(this),
    };

    const action = actionByKey[event.key];

    if (action) {
      action();
      return true;
    }
  }

  #move(amount) {
    const selectedPath = getSelectedPath();
    selectedPath.position = selectedPath.position.add(amount);
  }

  moveLeft() {
    this.#move(new Point(-1 * AMOUNT_IN_PIXELS, 0));
  }

  moveUp() {
    this.#move(new Point(0, -1 * AMOUNT_IN_PIXELS));
  }

  moveRight() {
    this.#move(new Point(AMOUNT_IN_PIXELS, 0));
  }

  moveDown() {
    this.#move(new Point(0, AMOUNT_IN_PIXELS));
  }
}

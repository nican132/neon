/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { Point } from 'paper';

import { scaleRadius } from './point-radius.js';

import { getSelectedPath, hitTestPoint } from '../paper/get-selected-item.js';

/**
 * Scales a path up or down in size.
 */
export class PathScaler {
  #scaling = false;

  onMouseMove(event, overrides, snapEvent) {
    if (!this.#scaling) {
      return;
    }

    const scalePoint = new Point(0, 0);
    const originalDistance = scalePoint.getDistance(overrides.snap2Grid ? snapEvent.lastSnapPoint : event.lastPoint, /* squared= */ false);
    const curDistance = scalePoint.getDistance(overrides.snap2Grid ? snapEvent.snapPoint : event.point, /* squared= */ false);

    const scaleFactor = curDistance / originalDistance;

    const path = getSelectedPath();
    path.scale(scaleFactor, scalePoint);
    scaleRadius(path, scaleFactor);

    return true;
  }

  onMouseDown(event, overrides) {
    if (!event.event.shiftKey && !overrides.shiftKey) {
      return;
    }

    if (hitTestPoint(event.point)) {
      return;
    }

    this.#scaling = true;
    return true;
  }

  onMouseUp() {
    this.#scaling = false;
  }
}

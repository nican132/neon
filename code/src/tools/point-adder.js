/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { incRadiusIndexes } from './point-radius.js';

import { hitTestPoint } from '../paper/get-selected-item';

/**
 * Add a point (segment) between two existing points on a path.
 */
export class PointAdder {
  onMouseDown(event) {
    const hitResult = hitTestPoint(event.point);
    if (!hitResult || hitResult.segment) {
      return;
    }

    incRadiusIndexes(hitResult.item, hitResult.location.index + 1);
    const segment = hitResult.item.insert(hitResult.location.index + 1, event.point);
    segment.point.selected = true;

    return true;
  }
}

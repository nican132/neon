/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { getAllPaths } from '../paper/get-selected-item';

/**
 * Unsmooths all paths.
 */
export class PathUnsmooth {
  onMouseDown() {
    getAllPaths().forEach((path) => path.clearHandles());
  }
}

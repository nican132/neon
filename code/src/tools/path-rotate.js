/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */

import { getSelectedCenter } from '../paper/get-selected-item';

const AMOUNT_IN_DEGRESS = 1;

/**
 * Rotates a path about its "center".
 */
export class PathRotate {
  onKeyDown(event) {
    if (event.key === 'q') {
      this.rotateLeft();
      return true;
    }

    if (event.key === 'e') {
      this.rotateRight();
      return true;
    }
  }

  #rotate(amountInDegrees) {
    const { center, path } = getSelectedCenter();
    path.rotate(amountInDegrees, center);
  }

  rotateLeft() {
    this.#rotate(-1 * AMOUNT_IN_DEGRESS);
  }

  rotateRight() {
    this.#rotate(AMOUNT_IN_DEGRESS);
  }
}

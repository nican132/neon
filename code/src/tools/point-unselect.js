/* eslint-disable no-param-reassign */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { getSelectedPath } from '../paper/get-selected-item';

/**
 * Unselects all points because sometimes the previous tooling doesn't do a good job of cleaning up after itself.
 */
export class PointUnselect {
  onMouseUp() {
    getSelectedPath().segments.forEach((segment) => { segment.point.selected = false; });
  }
}

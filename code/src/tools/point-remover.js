/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { hitTestPoint } from '../paper/get-selected-item.js';

import { removeRadiusIndex } from './point-radius.js';

/**
 * Remove a point anywhere along a path.
 */
export class PointRemover {
  onMouseDown(event, overrides) {
    if (!event.event.ctrlKey && !overrides.ctrlKey) {
      return;
    }

    const hitResult = hitTestPoint(event.point);
    if (!hitResult || !hitResult.segment) {
      return;
    }

    removeRadiusIndex(hitResult.item, hitResult.segment.index);
    hitResult.segment.remove();

    return true;
  }
}

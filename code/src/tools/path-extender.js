/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { incRadiusIndexes, setRadiusInMm } from './point-radius.js';

import { getSelectedPath, hitTestPoint } from '../paper/get-selected-item.js';

/**
 * Add a point (segment) to a path's head or tail.
 */
export class PathExtender {
  onMouseDown(event, overrides) {
    const selectedPath = getSelectedPath();
    if (!selectedPath) {
      return;
    }

    // This is a rare case where we must do both selected path and hitTestPoint as seperate functions.
    const hitResult = hitTestPoint(event.point);
    if (hitResult) {
      return;
    }

    const addPoint = () => {
      if (event.event.ctrlKey || overrides.ctrlKey) {
        incRadiusIndexes(selectedPath, 0);
        return selectedPath.insert(0, event.point);
      }
      return selectedPath.add(event.point);
    };

    const segment = addPoint();
    segment.point.selected = true;

    const neighbor = segment.isLast() ? segment.previous : segment.next;
    if (neighbor) {
      setRadiusInMm(selectedPath, neighbor.index, 0);
    }

    return true;
  }
}

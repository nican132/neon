/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */

/**
 * Triggers a listener when there is a click or keyboard.
 */
export class Click {
  #clickListener = () => { };
  #isUp;

  constructor(isUp = true) {
    this.#isUp = isUp;
  }

  setClickListener(listener) {
    this.#clickListener = listener;
  }

  onMouseUp() {
    if (!this.#isUp) {
      return;
    }

    this.#clickListener();
  }

  onKeyUp() {
    if (!this.#isUp) {
      return;
    }

    this.#clickListener();
  }

  onMouseDown() {
    if (this.#isUp) {
      return;
    }

    this.#clickListener();
  }

  onKeyDown() {
    if (this.#isUp) {
      return;
    }

    this.#clickListener();
  }
}

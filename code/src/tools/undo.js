/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import paper from 'paper';
import { isEqual } from 'lodash';
import { getOrCreateLayer } from '../paper/layer-management';

const isCtrlZ = (event) => event.key === 'z' && event.ctrlKey;

/**
 * Add a point (segment) between two existing points on a path.
 */
export class Undo {
  #history = [];
  #popListener = () => { };

  store() {
    const last = this.#history.at(-1);
    const layer = paper.project.activeLayer;
    const current = { name: layer.name, json: layer.exportJSON() };
    if (isEqual(last, current)) {
      return;
    }

    this.#history.push(current);
  }

  pop() {
    if (this.#history.length === 1) {
      return;
    }

    this.#history.pop();
    const last = this.#history.at(-1);
    getOrCreateLayer(last.name).importJSON(last.json);

    this.#popListener();
  }

  setPopListener(listener) {
    this.#popListener = listener;
  }

  onMouseUp() {
    this.store();
  }

  onKeyUp(event) {
    if (isCtrlZ(event)) {
      return;
    }

    this.store();
  }

  onKeyDown(event) {
    if (!isCtrlZ(event)) {
      return;
    }

    this.pop();
  }
}

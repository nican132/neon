/* eslint-disable no-param-reassign */
/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import paper from 'paper';

import { makePath } from '../factory/path-factory.js';
import { getCanvasWidth } from '../paper/scale-conversion.js';
import { MAX_PATHS } from '../views/ui-events.js';

import { getAllPaths, getSelectedPath } from '../paper/get-selected-item.js';

const handlePaste = (paste) => {
  if (!paste) {
    return;
  }

  const parsePasteWrapper = (data) => {
    try {
      return JSON.parse(data);
    } catch (e) {
      console.error(`Couldn't parse paste: ${data}`, e);
    }
  };

  const pasteWrapper = parsePasteWrapper(paste);
  if (!pasteWrapper) {
    return;
  }

  const loadedWidth = pasteWrapper.width;
  const canvasWidth = getCanvasWidth();
  const scaleFacter = canvasWidth / loadedWidth;

  const paths = getAllPaths();
  if (paths.length >= MAX_PATHS) {
    alert('You cannot add another path to the project.');
    return;
  }

  const clone = makePath();
  paths.forEach((path) => { path.selected = false; });
  clone.importJSON(pasteWrapper.path); // All other clone methods aren't deep.
  clone.scale(scaleFacter, new paper.Point(0, 0));
  clone.name = `${clone.id} paste`;
  clone.selected = true;

  // Should probably try/catch that importJSON and gracefully handle failures. Oh well.
};

/**
 * Triggers a copy or paste action
 */
export class CopyPaste {
  constructor() {
    document.addEventListener('paste', (event) => {
      if (event.target.nodeName === 'INPUT') {
        return;
      }
      event.preventDefault();

      const paste = (event.clipboardData || window.clipboardData).getData('text');
      handlePaste(paste);
    });
  }

  onKeyDown(event, overrides) {
    if (!event.ctrlKey && !overrides.ctrlKey) {
      return;
    }

    if (event.key === 'c') {
      const path = getSelectedPath();
      const paste = {
        width: getCanvasWidth(),
        path: path.exportJSON(),
      };
      navigator.clipboard.writeText(JSON.stringify(paste));
      return true;
    }

    if (event.key === 'v') {
      return true;
    }
  }
}

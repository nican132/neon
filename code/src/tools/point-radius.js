/* eslint-disable no-param-reassign */
/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { getNearestGridPixel } from '../augments/grid-draw.js';

import { getSelectedPoint, hitTestPoint } from '../paper/get-selected-item.js';
import { NEON_BEND_RADIUS_IN_MM, scalePixelsToMm } from '../paper/scale-conversion.js';

const MIN_BEND_RADIUS_MM = NEON_BEND_RADIUS_IN_MM;
const MAX_BEND_RADIUS_MM = NEON_BEND_RADIUS_IN_MM * 35; // picked at random :D

export const scaleRadius = (path, scaleFactor) => {
  path.data.segmentRadius = Object.keys(path.data.segmentRadius || {})
    .reduce((acc, cur) => ({
      ...acc,
      [cur]: path.data.segmentRadius[cur] * scaleFactor,
    }), {});
};

export const setRadiusInMm = (path, index, value) => {
  path.data.segmentRadius = path.data.segmentRadius || {};
  path.data.segmentRadius[index] = value;
};

export const getRadiusInMm = (path, index) => path.data.segmentRadius?.[index];

export const incRadiusIndexes = (path, startIndex, amount = 1) => {
  path.data.segmentRadius = Object.keys(path.data.segmentRadius || {})
    .reduce((acc, cur) => {
      const curInt = parseInt(cur, 10);
      const index = curInt + (curInt >= startIndex ? amount : 0);
      const value = path.data.segmentRadius[cur];
      return { ...acc, [index]: value };
    }, {});
};

export const removeRadiusIndex = (path, index) => {
  if (Object.keys(path.data.segmentRadius || {}).includes(`${index}`)) {
    delete path.data.segmentRadius[index];
  }
  incRadiusIndexes(path, index, -1);
};

/**
 * Class that lets you change the bend radius of a point.
 */
export class PointRadius {
  #radiusing;

  onMouseMove(event, overrides) {
    if (!this.#radiusing) {
      return;
    }

    const selected = getSelectedPoint();
    if (!selected) {
      return;
    }

    const { previous, next } = selected.segment;
    const vecA = previous.point.subtract(selected.point);
    const vecB = next.point.subtract(selected.point);
    const vecMouse = event.point.subtract(selected.point);

    const vectorAngle = (180 + vecA.getDirectedAngle(vecB));
    const mouseAngle = (180 + vecA.getDirectedAngle(vecMouse));

    const isVectorsObtuse = vectorAngle > 180;
    const isMouseObtuse = mouseAngle > 180;
    if (isVectorsObtuse !== isMouseObtuse
      || (isVectorsObtuse && vectorAngle < mouseAngle)
      || (!isVectorsObtuse && vectorAngle > mouseAngle)) {
      return true; // Mouse isn't on the same side as the angle.
    }

    const distanceInPixels = selected.point.getDistance(event.point) * 2.0;
    const distanceInMm = scalePixelsToMm(overrides.snap2Grid ? getNearestGridPixel(distanceInPixels) : distanceInPixels) - 1;
    const distanceInMmClamped = Math.max(MIN_BEND_RADIUS_MM, Math.min(MAX_BEND_RADIUS_MM, distanceInMm));

    setRadiusInMm(selected.path, selected.segment.index, distanceInMmClamped);
    return true;
  }

  onMouseDown(event, overrides) {
    if (!event.event.shiftKey && !overrides.shiftKey) {
      return;
    }

    const hitResult = hitTestPoint(event.point);
    if (!hitResult || !hitResult.segment || hitResult.segment.isFirst() || hitResult.segment.isLast()) {
      return;
    }

    this.#radiusing = true;
    hitResult.segment.point.selected = true;
    return true;
  }

  onMouseUp() {
    this.#radiusing = false;
  }
}

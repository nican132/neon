/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { Tool, Point } from 'paper';

import { getNearestGridPixel } from '../augments/grid-draw.js';

/**
 * Paper.js wants only one tool active at a time. But we can be smart and allow
 * multiple tools at the same time, depending on context. So this class will juggle
 * all of that information and ensure our tools are correctly called.
 */
export class ToolManager {
  #tool = new Tool();
  #subTools = [];

  #isControlOverride;
  #isShiftOverride;
  #isSnap2Grid;

  #isMouseDown;

  /**
   * Will setup a new Paper.js Tool.
   *
   * Subtools should be simple objects {onMouseMove, onMouseDown, onMouseUp}. Dont make them Paper.js Tools.
   * If they return true, then events will stop propogating. So install the tools in the order you care about.
   */
  constructor(subTools) {
    this.#subTools.push(...subTools);
    this.#tool.onMouseMove = this.#onMouseMove.bind(this);
    this.#tool.onMouseUp = this.#onMouseUp.bind(this);
    this.#tool.onMouseDown = this.#onMouseDown.bind(this);

    // Don't use #tool.onKeyDown since it can't handle multiple keys simultaneously.
    window.addEventListener('keydown', this.#onKeyDown.bind(this));
    window.addEventListener('keyup', this.#onKeyUp.bind(this));

    window.addEventListener('wheel', this.#onMouseWheel.bind(this), { passive: false });
  }

  #onMouseMove(event) {
    const isMouseDown = event.event.buttons === 1 || event.event.targetTouches?.length === 1;
    if (!isMouseDown) {
      return;
    }

    this.#subTools.find((tool) => tool.onMouseMove?.(event, this.#overrideEvent(event), this.#snapEvents(event)));
  }

  #onMouseDown(event) {
    if (event.event.button !== 0 && event.event.targetTouches?.length !== 1) {
      return;
    }

    this.#isMouseDown = true;
    event.event.target.focus();

    this.#subTools.find((tool) => tool.onMouseDown?.(event, this.#overrideEvent(event), this.#snapEvents(event)));
  }

  #onMouseUp(event) {
    if (event.event.button !== 0 && event.event.targetTouches?.length !== 0) {
      return;
    }

    this.#isMouseDown = false;

    this.#subTools.forEach((tool) => tool.onMouseUp?.(event, this.#overrideEvent(event), this.#snapEvents(event)));
  }

  #onKeyDown(event) {
    if (event.target.nodeName === 'INPUT') {
      return;
    }

    this.#subTools.find((tool) => tool.onKeyDown?.(event, this.#overrideEvent(event)));
  }

  #onKeyUp(event) {
    if (event.target.nodeName === 'INPUT') {
      return;
    }

    this.#subTools.forEach((tool) => tool.onKeyUp?.(event, this.#overrideEvent(event)));
  }

  #onMouseWheel(event) {
    if (event.target.nodeName !== 'CANVAS') {
      return;
    }
    event.preventDefault();

    this.#subTools.find((tool) => tool.onMouseWheel?.(event, this.#overrideEvent(event)));
  }

  #overrideEvent() {
    return {
      shiftKey: this.#isShiftOverride,
      ctrlKey: this.#isControlOverride,
      snap2Grid: this.#isSnap2Grid,
    };
  }

  #snapEvents(event) {
    if (!this.#isSnap2Grid) {
      return;
    }

    const lastSnapPoint = new Point(getNearestGridPixel(event.lastPoint.x), getNearestGridPixel(event.lastPoint.y));
    const snapPoint = new Point(getNearestGridPixel(event.point.x), getNearestGridPixel(event.point.y));

    return {
      lastSnapPoint,
      snapPoint,
      deltaSnapPoint: snapPoint.subtract(lastSnapPoint),
    };
  }

  setControlOverride(isControl) {
    this.#isControlOverride = isControl;
  }

  setShiftOverride(isShift) {
    this.#isShiftOverride = isShift;
  }

  setSnap2Grid(isSnap2Grid) {
    this.#isSnap2Grid = isSnap2Grid;
  }

  isMouseDown() {
    return this.#isMouseDown;
  }
}

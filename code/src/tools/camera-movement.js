/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import paper, { Point } from 'paper';
import { clamp, lowerCase } from 'lodash';

import { getCanvasWidth, scaleMmToPixels } from '../paper/scale-conversion.js';

const AMOUNT_IN_PIXELS = scaleMmToPixels(10); // How much to move when pressing WASD keys.

const clampAndSetCenter = (adjustedCenter) => {
  const limitInPixels = getCanvasWidth();
  const clampedCenter = new Point(clamp(adjustedCenter.x, 0, limitInPixels), clamp(adjustedCenter.y, 0, limitInPixels));
  paper.view.center = clampedCenter;
};

/**
 * Move the display around
 */
export class CameraMovement {
  #zoomExponent = 0;

  onKeyDown(event, overrides) {
    if (event.ctrlKey || overrides.ctrlKey) {
      return this.#onCtrlKeyDown(event, overrides);
    }

    if (event.shiftKey || overrides.shiftKey) {
      return this.#onShiftKeyDown(event, overrides);
    }

    return false;
  }

  #onShiftKeyDown(event) {
    const actionByKey = {
      a: this.moveLeft.bind(this),
      w: this.moveUp.bind(this),
      s: this.moveDown.bind(this),
      d: this.moveRight.bind(this),
    };

    const action = actionByKey[lowerCase(event.key)];

    if (action) {
      action();
      return true;
    }

    return false;
  }

  #onCtrlKeyDown(event) {
    if (event.key === '=' || event.key === '+') {
      // Zoom in and make stuff bigger.
      event.preventDefault();
      return this.#zoom(-100, paper.view.center);
    }

    if (event.key === '-') {
      // Zoom out and make stuff smaller.
      event.preventDefault();
      return this.#zoom(100, paper.view.center);
    }

    return false;
  }

  onMouseWheel(event) {
    const mousePosition = new Point(event.offsetX, event.offsetY);
    const viewPosition = paper.view.viewToProject(mousePosition);
    return this.#zoom(event.deltaY, viewPosition);
  }

  #zoom(deltaY, viewPosition) {
    const oldZoom = paper.view.zoom;
    const isZoomIn = deltaY < 0;
    const change = isZoomIn ? 1 : -1;
    const exponent = this.#zoomExponent + change;
    const newZoom = Math.exp(0.05 * exponent);
    const beta = newZoom / oldZoom;

    const min = -12; // this is the zoom out limit
    const max = 20; // this is the zoom in limit
    if (exponent < min || exponent > max) {
      return false;
    }

    this.#zoomExponent += change;
    paper.view.zoom = newZoom;

    if (isZoomIn) {
      const offset = paper.view.center.subtract(viewPosition);
      const adjustedCenter = paper.view.center.add(offset.multiply(1 - beta).multiply(2));
      clampAndSetCenter(adjustedCenter);
    } else if (exponent < 0) {
      const canvasCenter = new Point(getCanvasWidth() / 2.0, getCanvasWidth() / 2.0);
      const offset = paper.view.center.subtract(canvasCenter);
      const factor = exponent / min;
      const adjustedCenter = paper.view.center.add(offset.multiply(factor ** 3).multiply(-1));
      clampAndSetCenter(adjustedCenter);
    }

    paper.view.draw();

    return true;
  }

  #move(amount) {
    clampAndSetCenter(paper.view.center.add(amount));
  }

  moveLeft() {
    this.#move(new Point(-1 * AMOUNT_IN_PIXELS, 0));
  }

  moveUp() {
    this.#move(new Point(0, -1 * AMOUNT_IN_PIXELS));
  }

  moveRight() {
    this.#move(new Point(AMOUNT_IN_PIXELS, 0));
  }

  moveDown() {
    this.#move(new Point(0, AMOUNT_IN_PIXELS));
  }
}

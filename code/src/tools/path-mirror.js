/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { getSelectedCenter } from '../paper/get-selected-item';

/**
 * Mirrors a path horizontally or vertically
 */
export class PathMirror {
  onKeyDown(event) {
    if (event.key === 'ArrowRight' || event.key === 'ArrowLeft') {
      this.mirrorHorizontal();
      return true;
    }

    if (event.key === 'ArrowUp' || event.key === 'ArrowDown') {
      this.mirrorVertical();
      return true;
    }
  }

  #mirror(x, y) {
    const { center, path } = getSelectedCenter();
    path.scale(x, y, center);
  }

  mirrorHorizontal() {
    this.#mirror(-1, 1);
  }

  mirrorVertical() {
    this.#mirror(1, -1);
  }
}

/* eslint-disable consistent-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
import { getSelectedPoint, hitTestPoint } from '../paper/get-selected-item.js';

const getNextPoint = (event, overrides, snapEvents) => {
  if (overrides.snap2Grid) {
    return snapEvents.snapPoint.clone();
  }

  return event.point.clone();
};

/**
 * Class is a tool that lets you click and drag segment points.
 */
export class PointMover {
  onMouseMove(event, overrides, snapEvents) {
    const selected = getSelectedPoint();
    if (!selected) {
      return;
    }

    const point = getNextPoint(event, overrides, snapEvents);

    const minDistance = 0.0001;
    if (selected.segment.previous?.point.getDistance(point) < minDistance
      || selected.segment.next?.point.getDistance(point) < minDistance) {
      // Don't want an exact match, this means two points are overlapping.
      return;
    }

    selected.segment.point = point;
    return true;
  }

  onMouseDown(event) {
    const hitResult = hitTestPoint(event.point);
    if (!hitResult || !hitResult.segment) {
      return;
    }

    hitResult.segment.point.selected = true;
    return true;
  }
}

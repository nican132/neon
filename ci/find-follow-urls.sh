#!/bin/bash
# I am terrified that tinyurl will delete all of my redicts inside gifts.txt
#
# This file will run a query against all the urls and follow them then print
# out their final destination in a file (which should include the raw path data).
# 
# Just incase tinyurl goes away, then I'll have a log/backup of the final data.

apt-get update

# Install curl
if ! command -v curl &> /dev/null
then
    apt-get install curl -y
fi

file=../redirects.txt

cat ../gifts.txt | grep -Eo "(http|https)://tinyurl[a-zA-Z0-9./?=_%:-]*" | while read -r line ; do
    echo "Processing $line"
    echo "$line" >> $file
    echo "---" >> $file
    curl -w "%{url_effective}\n" -I -L -s -S $line -o /dev/null >> $file
    echo "" >> $file
done

cat $file
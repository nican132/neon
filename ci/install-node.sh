#!/bin/bash
set -x #echo on

apt-get update

# Install curl
if ! command -v curl &> /dev/null
then
    apt-get install curl -y
fi

# Standard nodejs install
if ! command -v npm &> /dev/null
then
    curl -sL https://deb.nodesource.com/setup_17.x | bash -
    apt-get install nodejs -y
fi

# Setup for...reasons
npm config set prefix /usr/local

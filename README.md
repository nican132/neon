# Neon

Play with it https://kobaj.gitlab.io/neon/

## Description

This is a little tool for helping you design and 3d print tracks for wires and [Led Neon](https://www.google.com/search?q=led+neon) to fit inside of. See the full documentation in the wiki: https://gitlab.com/kobaj/neon/-/wikis/Help

## Badges
Code is licensed under [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

However, the characters and artwork are licensed under [![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC_BY--NC--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Development

To get started developing you need to 
1. Install [Node 16.14.0](https://nodejs.org/en/)
1. Run `npm install` in the same dir as this readme
1. Run `npm run start` to load a browser with hot replaced code